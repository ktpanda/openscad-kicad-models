include <../ktpanda/consts.scad>
use <../ktpanda/funcs.scad>
use <../ktpanda/shapes.scad>
use <kicad-models.scad>
use <wrl-models/kicad-models-wrl.scad>

module component_xform(data, bt=1.6) {
    pos = data[1];
    rot = data[2];
    side = data[3];

    translate(pos)
    if (side == "bottom") {
        tz(-eps) rz(rot+180) ry(180)  children();
    } else {
        tz(bt+eps) rz(rot) children();
    }
}

module component_unxform(data, bt=1.6) {
    pos = data[1];
    rot = data[2];
    side = data[3];


    if (side == "bottom") {
        ry(-180) rz(-rot-180) tz(eps) translate(-pos) children();
    } else {
         rz(-rot) tz(-bt-eps) translate(-pos) children();
    }
}

module kicad_component() {
    if ($cpackage == "TO-220-3_Vertical") { power_mosfet(); }

    else if ($cpackage == "pin_header_vertical") { pinheader_vert(r=$cargs[0], n=$cargs[1]); }
    else if ($cpackage == "pin_header_horizontal") { pinheader_horiz(r=$cargs[0], n=$cargs[1]); }
    else if ($cpackage == "pin_socket_vertical") { pinsocket_vert(r=$cargs[0], n=$cargs[1]); }
    else if ($cpackage == "screw_terminal") { screw_terminal(n=$cargs[0]); }
    else if ($cpackage == "spade_terminal") { spade_terminal(size=$cargs[0], r=$cargs[1], n=$cargs[2], spc=$cargs[3]); }
    else if ($cpackage == "smd_r") { smd_resistor(size=$cargs[0]); }
    else if ($cpackage == "smd_d") { smd_diode(size=$cargs[0]); }
    else if ($cpackage == "smd_c") { smd_capacitor(size=$cargs[0]); }
    else if ($cpackage == "smd_led") { smd_led(size=$cargs[0]); }
    else if ($cpackage == "qfn") { smd_qfn(pins=$cargs[0], w=$cargs[1], h=$cargs[2]); }
    else if ($cpackage == "soic") { smd_soic(pins=$cargs[0], wide=$cargs[1], w=$cargs[2], h=$cargs[3], spc=$cargs[4]); }
    else if ($cpackage == "ssop") { smd_ssop(pins=$cargs[0], w=$cargs[1], h=$cargs[2], spc=$cargs[3]); }
    else if ($cpackage == "tssop") { smd_tssop(pins=$cargs[0], w=$cargs[1], h=$cargs[2], spc=$cargs[3]); }
    else if ($cpackage == "dip") { tht_dip(pins=$cargs[0], w=$cargs[1]); }

    else if ($cpackage == "SOT-223-3_TabPin2") { kicad_sot_223(); }
    else if ($cpackage == "SOT-23") { kicad_sot_23(); }

    else if ($cpackage == "SW_SPST_B3U-1000P") { kicad_sw_spst_b3u_1000p (); }
    else if ($cpackage == "USB_Micro-B_Molex_47346-0001") { kicad_usb_micro_b_molex_47346_0001(); }
    else if ($cpackage == "ESP-12E") { kicad_esp_12e(); }

    else if ($cpackage == "SW_PUSH_6mm") { kicad_sw_push_6mm(); }

    else if ($cpackage == "pin_header_horizontal") { pinheader_horiz(r=$cargs[0], n=$cargs[1]); }
    else if ($cpackage == "electrolytic_capacitor") { electrolytic_capacitor(d=$cargs[0], h=12.5, pin_spacing=$cargs[1]); }
    else if ($cpackage == "MagneticBuzzer_ProSignal_ABI-010-RC") { kicad_magneticbuzzer_prosignal_abi_010_rc(); }
    else if ($cpackage == "USB_A_Wuerth_61400826021_Horizontal_Stacked") { wuerth_dual_usb_a_connector(); }

    else if ($cpackage == "Relay_SPDT_Songle_SRD") { relay_songle_srd(); }

    else if ($cpackage == "BarrelJack_Horizontal") { barrel_jack(); }
    else if ($cpackage == "LED_RGB_5050-6") { kicad_led_rgb_5050_6 (); }
    else { echo(str("Unknown: else if ($cpackage == \"", $cpackage, "\") { kicad_ (); }")); }
}

module iter_drill_holes(drill_holes, min_hole_size=0, hole_size_offset=0) {
    for($hole = drill_holes) {
        $orig_hole_size = $hole[1];
        $hole_size = $orig_hole_size + hole_size_offset;
        if($hole_size > min_hole_size)
        if (is_list($hole[0][0])) {
            hull() {
                translate($hole[0][0]) children();
                translate($hole[0][1]) children();
            }
        } else {
            translate($hole[0]) children();
        }
    }
}

module pcb_drill(drill_holes, min_hole_size=0, hole_size_offset=0) {
    iter_drill_holes(drill_holes, min_hole_size, hole_size_offset) {
        circle(d=$hole_size);
    }
}

module pcb_model(prefix, origin, drill, traces=false, thick=1.6, trace_thick=0.3, convexity=10, pcb_color="#00bb00", trace_color="#00cc00", min_hole_size=0, hole_size_offset=0) {
    color(pcb_color)
    linear_extrude(thick)
    render(convexity)
    difference() {
        translate(-origin) import(str(prefix, "-pcb-board.dxf"));
        pcb_drill(drill, min_hole_size, hole_size_offset);
    }

    if (traces) {
        color(trace_color, 0.5)
        translate([0, 0, -trace_thick-0.001])
        linear_extrude(trace_thick)
        render(convexity)
        difference() {
            translate(-origin) import(str(prefix, "-pcb-trace-b.dxf"));
            pcb_drill(drill, min_hole_size, hole_size_offset);
        }

        color(trace_color, 0.5)
        translate([0, 0, thick+0.001])
        linear_extrude(trace_thick)
        render(convexity)
        difference() {
            translate(-origin) import(str(prefix, "-pcb-trace-f.dxf"));
            pcb_drill(drill, min_hole_size, hole_size_offset);
        }
    }
}

module iter_objects(objects, bt=1.6) {
    for ($cdata = objects) {
        $ref = $cdata[0];
        $cpos = $cdata[1];
        $crot = $cdata[2];
        $cside = $cdata[3];
        $cval = $cdata[4];
        $cpackage = $cdata[5];
        $cargs = $cdata[6];
        component_xform($cdata, bt) children();
    }
}

module label_objects(objects, coords=false) {
    for (o = objects) {
        data = o[1];
        translate(data[0]) {
            lbl = coords ? str(o[0], " ", data[0]) : o[0];
            color("blue")
            cylx(0, 0.1, d=1);
            tx(.6) linear_extrude(0.1) text(lbl, size=1, valign="center");
        }
    }
}
