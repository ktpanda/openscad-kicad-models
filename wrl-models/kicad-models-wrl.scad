module kicad_sot_23() {
    color("#252424") import("kicad_sot_23-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_sot_23-D2D1C7.stl", convexity=10);
}

module kicad_sot_223() {
    color("#252424") import("kicad_sot_223-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_sot_223-D2D1C7.stl", convexity=10);
}

module kicad_sw_spst_b3u_1000p() {
    color("#D2D1C7") import("kicad_sw_spst_b3u_1000p-D2D1C7.stl", convexity=10);
    color("#B0A998") import("kicad_sw_spst_b3u_1000p-B0A998.stl", convexity=10);
    color("#252424") import("kicad_sw_spst_b3u_1000p-252424.stl", convexity=10);
}

module kicad_magneticbuzzer_prosignal_abi_010_rc() {
    color("#252424") import("kicad_magneticbuzzer_prosignal_abi_010_rc-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_magneticbuzzer_prosignal_abi_010_rc-D2D1C7.stl", convexity=10);
}

module kicad_hdmi_a_molex_208658_1001_horizontal() {
    color("#818181") import("kicad_hdmi_a_molex_208658_1001_horizontal-818181.stl", convexity=10);
    color("#252424") import("kicad_hdmi_a_molex_208658_1001_horizontal-252424.stl", convexity=10);
    color("#DBBC7E") import("kicad_hdmi_a_molex_208658_1001_horizontal-DBBC7E.stl", convexity=10);
}

module kicad_usb_micro_b_molex_47346_0001() {
    color("#888888") import("kicad_usb_micro_b_molex_47346_0001-888888.stl", convexity=10);
}

module kicad_usb_c_plug_shenzhenjingtuojin_918_118a2021y40002_vertical() {
    color("#888888") import("kicad_usb_c_plug_shenzhenjingtuojin_918_118a2021y40002_vertical-888888.stl", convexity=10);
}

module kicad_usb_c_receptacle_gct_usb4105_xx_a_16p_topmnt_horizontal() {
    color("#DBBC7E") import("kicad_usb_c_receptacle_gct_usb4105_xx_a_16p_topmnt_horizontal-DBBC7E.stl", convexity=10);
    color("#454545") import("kicad_usb_c_receptacle_gct_usb4105_xx_a_16p_topmnt_horizontal-454545.stl", convexity=10);
    color("#D2D1C7") import("kicad_usb_c_receptacle_gct_usb4105_xx_a_16p_topmnt_horizontal-D2D1C7.stl", convexity=10);
}

module kicad_usb_c_receptacle_gct_usb4135_gf_a_6p_topmnt_horizontal() {
    color("#DBBC7E") import("kicad_usb_c_receptacle_gct_usb4135_gf_a_6p_topmnt_horizontal-DBBC7E.stl", convexity=10);
    color("#D2D1C7") import("kicad_usb_c_receptacle_gct_usb4135_gf_a_6p_topmnt_horizontal-D2D1C7.stl", convexity=10);
    color("#454545") import("kicad_usb_c_receptacle_gct_usb4135_gf_a_6p_topmnt_horizontal-454545.stl", convexity=10);
}

module kicad_r_0201_0603metric() {
    color("#D2D1C7") import("kicad_r_0201_0603metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_0201_0603metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_0201_0603metric-141517.stl", convexity=10);
}

module kicad_r_0402_1005metric() {
    color("#D2D1C7") import("kicad_r_0402_1005metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_0402_1005metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_0402_1005metric-141517.stl", convexity=10);
}

module kicad_r_0603_1608metric() {
    color("#D2D1C7") import("kicad_r_0603_1608metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_0603_1608metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_0603_1608metric-141517.stl", convexity=10);
}

module kicad_r_0612_1632metric() {
    color("#D2D1C7") import("kicad_r_0612_1632metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_0612_1632metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_0612_1632metric-141517.stl", convexity=10);
}

module kicad_r_0805_2012metric() {
    color("#D2D1C7") import("kicad_r_0805_2012metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_0805_2012metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_0805_2012metric-141517.stl", convexity=10);
}

module kicad_r_0815_2038metric() {
    color("#D2D1C7") import("kicad_r_0815_2038metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_0815_2038metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_0815_2038metric-141517.stl", convexity=10);
}

module kicad_r_1020_2550metric() {
    color("#D2D1C7") import("kicad_r_1020_2550metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_1020_2550metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_1020_2550metric-141517.stl", convexity=10);
}

module kicad_r_1206_3216metric() {
    color("#D2D1C7") import("kicad_r_1206_3216metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_1206_3216metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_1206_3216metric-141517.stl", convexity=10);
}

module kicad_r_1210_3225metric() {
    color("#D2D1C7") import("kicad_r_1210_3225metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_1210_3225metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_1210_3225metric-141517.stl", convexity=10);
}

module kicad_r_1218_3246metric() {
    color("#D2D1C7") import("kicad_r_1218_3246metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_1218_3246metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_1218_3246metric-141517.stl", convexity=10);
}

module kicad_r_1806_4516metric() {
    color("#D2D1C7") import("kicad_r_1806_4516metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_1806_4516metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_1806_4516metric-141517.stl", convexity=10);
}

module kicad_r_1812_4532metric() {
    color("#D2D1C7") import("kicad_r_1812_4532metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_1812_4532metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_1812_4532metric-141517.stl", convexity=10);
}

module kicad_r_2010_5025metric() {
    color("#D2D1C7") import("kicad_r_2010_5025metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_2010_5025metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_2010_5025metric-141517.stl", convexity=10);
}

module kicad_r_2512_6332metric() {
    color("#D2D1C7") import("kicad_r_2512_6332metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_2512_6332metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_2512_6332metric-141517.stl", convexity=10);
}

module kicad_r_2816_7142metric() {
    color("#D2D1C7") import("kicad_r_2816_7142metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_2816_7142metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_2816_7142metric-141517.stl", convexity=10);
}

module kicad_r_4020_10251metric() {
    color("#D2D1C7") import("kicad_r_4020_10251metric-D2D1C7.stl", convexity=10);
    color("#E4E3CF") import("kicad_r_4020_10251metric-E4E3CF.stl", convexity=10);
    color("#141517") import("kicad_r_4020_10251metric-141517.stl", convexity=10);
}

module kicad_d_0603_1608metric() {
    color("#D2D1C7") import("kicad_d_0603_1608metric-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_d_0603_1608metric-252424.stl", convexity=10);
    color("#B0A998") import("kicad_d_0603_1608metric-B0A998.stl", convexity=10);
}

module kicad_d_0805_2012metric() {
    color("#D2D1C7") import("kicad_d_0805_2012metric-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_d_0805_2012metric-252424.stl", convexity=10);
    color("#B0A998") import("kicad_d_0805_2012metric-B0A998.stl", convexity=10);
}

module kicad_d_1206_3216metric() {
    color("#D2D1C7") import("kicad_d_1206_3216metric-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_d_1206_3216metric-252424.stl", convexity=10);
    color("#B0A998") import("kicad_d_1206_3216metric-B0A998.stl", convexity=10);
}

module kicad_d_2010_5025metric() {
    color("#D2D1C7") import("kicad_d_2010_5025metric-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_d_2010_5025metric-252424.stl", convexity=10);
    color("#B0A998") import("kicad_d_2010_5025metric-B0A998.stl", convexity=10);
}

module kicad_d_2114_3652metric() {
    color("#D2D1C7") import("kicad_d_2114_3652metric-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_d_2114_3652metric-252424.stl", convexity=10);
    color("#B0A998") import("kicad_d_2114_3652metric-B0A998.stl", convexity=10);
}

module kicad_d_3220_8050metric() {
    color("#D2D1C7") import("kicad_d_3220_8050metric-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_d_3220_8050metric-252424.stl", convexity=10);
    color("#B0A998") import("kicad_d_3220_8050metric-B0A998.stl", convexity=10);
}

module kicad_c_0201_0603metric() {
    color("#D2D1C7") import("kicad_c_0201_0603metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_0201_0603metric-604436.stl", convexity=10);
}

module kicad_c_0402_1005metric() {
    color("#D2D1C7") import("kicad_c_0402_1005metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_0402_1005metric-604436.stl", convexity=10);
}

module kicad_c_0603_1608metric() {
    color("#D2D1C7") import("kicad_c_0603_1608metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_0603_1608metric-604436.stl", convexity=10);
}

module kicad_c_0805_2012metric() {
    color("#D2D1C7") import("kicad_c_0805_2012metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_0805_2012metric-604436.stl", convexity=10);
}

module kicad_c_1206_3216metric() {
    color("#D2D1C7") import("kicad_c_1206_3216metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_1206_3216metric-604436.stl", convexity=10);
}

module kicad_c_1210_3225metric() {
    color("#D2D1C7") import("kicad_c_1210_3225metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_1210_3225metric-604436.stl", convexity=10);
}

module kicad_c_1806_4516metric() {
    color("#D2D1C7") import("kicad_c_1806_4516metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_1806_4516metric-604436.stl", convexity=10);
}

module kicad_c_1812_4532metric() {
    color("#D2D1C7") import("kicad_c_1812_4532metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_1812_4532metric-604436.stl", convexity=10);
}

module kicad_c_1825_4564metric() {
    color("#D2D1C7") import("kicad_c_1825_4564metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_1825_4564metric-604436.stl", convexity=10);
}

module kicad_c_2010_5025metric() {
    color("#D2D1C7") import("kicad_c_2010_5025metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_2010_5025metric-604436.stl", convexity=10);
}

module kicad_c_2220_5650metric() {
    color("#D2D1C7") import("kicad_c_2220_5650metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_2220_5650metric-604436.stl", convexity=10);
}

module kicad_c_2225_5664metric() {
    color("#D2D1C7") import("kicad_c_2225_5664metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_2225_5664metric-604436.stl", convexity=10);
}

module kicad_c_2512_6332metric() {
    color("#D2D1C7") import("kicad_c_2512_6332metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_2512_6332metric-604436.stl", convexity=10);
}

module kicad_c_2816_7142metric() {
    color("#D2D1C7") import("kicad_c_2816_7142metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_2816_7142metric-604436.stl", convexity=10);
}

module kicad_c_3640_9110metric() {
    color("#D2D1C7") import("kicad_c_3640_9110metric-D2D1C7.stl", convexity=10);
    color("#604436") import("kicad_c_3640_9110metric-604436.stl", convexity=10);
}

module kicad_led_0201_0603metric() {
    color("#E4E3CF") import("kicad_led_0201_0603metric-E4E3CF.stl", convexity=10);
    color("#DBBC7E") import("kicad_led_0201_0603metric-DBBC7E.stl", convexity=10);
    color("#56AD71") import("kicad_led_0201_0603metric-56AD71.stl", convexity=10);
}

module kicad_led_0402_1005metric() {
    color("#E4E3CF") import("kicad_led_0402_1005metric-E4E3CF.stl", convexity=10);
    color("#DBBC7E") import("kicad_led_0402_1005metric-DBBC7E.stl", convexity=10);
    color("#56AD71") import("kicad_led_0402_1005metric-56AD71.stl", convexity=10);
}

module kicad_led_0603_1608metric() {
    color("#E4E3CF") import("kicad_led_0603_1608metric-E4E3CF.stl", convexity=10);
    color("#DBBC7E") import("kicad_led_0603_1608metric-DBBC7E.stl", convexity=10);
    color("#56AD71") import("kicad_led_0603_1608metric-56AD71.stl", convexity=10);
}

module kicad_led_0805_2012metric() {
    color("#E4E3CF") import("kicad_led_0805_2012metric-E4E3CF.stl", convexity=10);
    color("#DBBC7E") import("kicad_led_0805_2012metric-DBBC7E.stl", convexity=10);
    color("#56AD71") import("kicad_led_0805_2012metric-56AD71.stl", convexity=10);
}

module kicad_led_1206_3216metric() {
    color("#E4E3CF") import("kicad_led_1206_3216metric-E4E3CF.stl", convexity=10);
    color("#DBBC7E") import("kicad_led_1206_3216metric-DBBC7E.stl", convexity=10);
    color("#56AD71") import("kicad_led_1206_3216metric-56AD71.stl", convexity=10);
}

module kicad_led_1210_3225metric() {
    color("#E4E3CF") import("kicad_led_1210_3225metric-E4E3CF.stl", convexity=10);
    color("#DBBC7E") import("kicad_led_1210_3225metric-DBBC7E.stl", convexity=10);
    color("#56AD71") import("kicad_led_1210_3225metric-56AD71.stl", convexity=10);
}

module kicad_led_1806_4516metric() {
    color("#E4E3CF") import("kicad_led_1806_4516metric-E4E3CF.stl", convexity=10);
    color("#DBBC7E") import("kicad_led_1806_4516metric-DBBC7E.stl", convexity=10);
    color("#56AD71") import("kicad_led_1806_4516metric-56AD71.stl", convexity=10);
}

module kicad_led_1812_4532metric() {
    color("#E4E3CF") import("kicad_led_1812_4532metric-E4E3CF.stl", convexity=10);
    color("#DBBC7E") import("kicad_led_1812_4532metric-DBBC7E.stl", convexity=10);
    color("#56AD71") import("kicad_led_1812_4532metric-56AD71.stl", convexity=10);
}

module kicad_led_2010_5025metric() {
    color("#E4E3CF") import("kicad_led_2010_5025metric-E4E3CF.stl", convexity=10);
    color("#DBBC7E") import("kicad_led_2010_5025metric-DBBC7E.stl", convexity=10);
    color("#56AD71") import("kicad_led_2010_5025metric-56AD71.stl", convexity=10);
}

module kicad_led_2512_6332metric() {
    color("#E4E3CF") import("kicad_led_2512_6332metric-E4E3CF.stl", convexity=10);
    color("#DBBC7E") import("kicad_led_2512_6332metric-DBBC7E.stl", convexity=10);
    color("#56AD71") import("kicad_led_2512_6332metric-56AD71.stl", convexity=10);
}

module kicad_led_2816_7142metric() {
    color("#E4E3CF") import("kicad_led_2816_7142metric-E4E3CF.stl", convexity=10);
    color("#DBBC7E") import("kicad_led_2816_7142metric-DBBC7E.stl", convexity=10);
    color("#56AD71") import("kicad_led_2816_7142metric-56AD71.stl", convexity=10);
}

module kicad_led_rgb_5050_6() {
    color("#E4E3CF") import("kicad_led_rgb_5050_6-E4E3CF.stl", convexity=10);
    color("#2266B9") import("kicad_led_rgb_5050_6-2266B9.stl", convexity=10);
    color("#D2D1C7") import("kicad_led_rgb_5050_6-D2D1C7.stl", convexity=10);
}

module kicad_esp_12e() {
    color("#282828") import("kicad_esp_12e-282828.stl", convexity=10);
    color("#818181") import("kicad_esp_12e-818181.stl", convexity=10);
    color("#DBBC7E") import("kicad_esp_12e-DBBC7E.stl", convexity=10);
    color("#E4E3CF") import("kicad_esp_12e-E4E3CF.stl", convexity=10);
    color("#56AD71") import("kicad_esp_12e-56AD71.stl", convexity=10);
}

module kicad_sw_push_6mm() {
    color("#252424") import("kicad_sw_push_6mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_sw_push_6mm-D2D1C7.stl", convexity=10);
}

module kicad_soic_14w_7_5x9_0mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_14w_7_5x9_0mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_14w_7_5x9_0mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_14w_7_5x9_0mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_14_3_9x8_7mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_14_3_9x8_7mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_14_3_9x8_7mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_14_3_9x8_7mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_16w_12_7_5x10_3mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_16w_12_7_5x10_3mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_16w_12_7_5x10_3mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_16w_12_7_5x10_3mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_16w_5_3x10_2mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_16w_5_3x10_2mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_16w_5_3x10_2mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_16w_5_3x10_2mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_16w_7_5x10_3mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_16w_7_5x10_3mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_16w_7_5x10_3mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_16w_7_5x10_3mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_16w_7_5x12_8mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_16w_7_5x12_8mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_16w_7_5x12_8mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_16w_7_5x12_8mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_16_3_9x9_9mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_16_3_9x9_9mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_16_3_9x9_9mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_16_3_9x9_9mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_16_4_55x10_3mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_16_4_55x10_3mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_16_4_55x10_3mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_16_4_55x10_3mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_18w_7_5x11_6mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_18w_7_5x11_6mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_18w_7_5x11_6mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_18w_7_5x11_6mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_20w_7_5x12_8mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_20w_7_5x12_8mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_20w_7_5x12_8mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_20w_7_5x12_8mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_24w_7_5x15_4mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_24w_7_5x15_4mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_24w_7_5x15_4mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_24w_7_5x15_4mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_28w_7_5x17_9mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_28w_7_5x17_9mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_28w_7_5x17_9mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_28w_7_5x17_9mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_28w_7_5x18_7mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_28w_7_5x18_7mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_28w_7_5x18_7mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_28w_7_5x18_7mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_8_1ep_3_9x4_9mm_p1_27mm_ep2_35x2_35mm() {
    color("#D2D1C7") import("kicad_soic_8_1ep_3_9x4_9mm_p1_27mm_ep2_35x2_35mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_8_1ep_3_9x4_9mm_p1_27mm_ep2_35x2_35mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_8_1ep_3_9x4_9mm_p1_27mm_ep2_35x2_35mm-B0A998.stl", convexity=10);
}

module kicad_soic_8_n7_3_9x4_9mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_8_n7_3_9x4_9mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_8_n7_3_9x4_9mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_8_n7_3_9x4_9mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_8_3_9x4_9mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_8_3_9x4_9mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_8_3_9x4_9mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_8_3_9x4_9mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_soic_8_5_275x5_275mm_p1_27mm() {
    color("#D2D1C7") import("kicad_soic_8_5_275x5_275mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_soic_8_5_275x5_275mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_soic_8_5_275x5_275mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_ssop_10_3_9x4_9mm_p1_00mm() {
    color("#D2D1C7") import("kicad_ssop_10_3_9x4_9mm_p1_00mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_10_3_9x4_9mm_p1_00mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_10_3_9x4_9mm_p1_00mm-B0A998.stl", convexity=10);
}

module kicad_ssop_14_5_3x6_2mm_p0_65mm() {
    color("#D2D1C7") import("kicad_ssop_14_5_3x6_2mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_14_5_3x6_2mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_14_5_3x6_2mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_ssop_16_3_9x4_9mm_p0_635mm() {
    color("#D2D1C7") import("kicad_ssop_16_3_9x4_9mm_p0_635mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_16_3_9x4_9mm_p0_635mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_16_3_9x4_9mm_p0_635mm-B0A998.stl", convexity=10);
}

module kicad_ssop_16_4_4x5_2mm_p0_65mm() {
    color("#D2D1C7") import("kicad_ssop_16_4_4x5_2mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_16_4_4x5_2mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_16_4_4x5_2mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_ssop_16_5_3x6_2mm_p0_65mm() {
    color("#D2D1C7") import("kicad_ssop_16_5_3x6_2mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_16_5_3x6_2mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_16_5_3x6_2mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_ssop_18_4_4x6_5mm_p0_65mm() {
    color("#D2D1C7") import("kicad_ssop_18_4_4x6_5mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_18_4_4x6_5mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_18_4_4x6_5mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_ssop_20_3_9x8_7mm_p0_635mm() {
    color("#D2D1C7") import("kicad_ssop_20_3_9x8_7mm_p0_635mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_20_3_9x8_7mm_p0_635mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_20_3_9x8_7mm_p0_635mm-B0A998.stl", convexity=10);
}

module kicad_ssop_20_4_4x6_5mm_p0_65mm() {
    color("#D2D1C7") import("kicad_ssop_20_4_4x6_5mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_20_4_4x6_5mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_20_4_4x6_5mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_ssop_20_5_3x7_2mm_p0_65mm() {
    color("#D2D1C7") import("kicad_ssop_20_5_3x7_2mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_20_5_3x7_2mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_20_5_3x7_2mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_ssop_24_3_9x8_7mm_p0_635mm() {
    color("#D2D1C7") import("kicad_ssop_24_3_9x8_7mm_p0_635mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_24_3_9x8_7mm_p0_635mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_24_3_9x8_7mm_p0_635mm-B0A998.stl", convexity=10);
}

module kicad_ssop_24_5_3x8_2mm_p0_65mm() {
    color("#D2D1C7") import("kicad_ssop_24_5_3x8_2mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_24_5_3x8_2mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_24_5_3x8_2mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_ssop_28_3_9x9_9mm_p0_635mm() {
    color("#D2D1C7") import("kicad_ssop_28_3_9x9_9mm_p0_635mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_28_3_9x9_9mm_p0_635mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_28_3_9x9_9mm_p0_635mm-B0A998.stl", convexity=10);
}

module kicad_ssop_28_5_3x10_2mm_p0_65mm() {
    color("#D2D1C7") import("kicad_ssop_28_5_3x10_2mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_28_5_3x10_2mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_28_5_3x10_2mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_ssop_32_11_305x20_495mm_p1_27mm() {
    color("#D2D1C7") import("kicad_ssop_32_11_305x20_495mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_32_11_305x20_495mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_32_11_305x20_495mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_ssop_44_5_3x12_8mm_p0_5mm() {
    color("#D2D1C7") import("kicad_ssop_44_5_3x12_8mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_44_5_3x12_8mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_44_5_3x12_8mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_ssop_48_7_5x15_9mm_p0_635mm() {
    color("#D2D1C7") import("kicad_ssop_48_7_5x15_9mm_p0_635mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_48_7_5x15_9mm_p0_635mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_48_7_5x15_9mm_p0_635mm-B0A998.stl", convexity=10);
}

module kicad_ssop_56_7_5x18_5mm_p0_635mm() {
    color("#D2D1C7") import("kicad_ssop_56_7_5x18_5mm_p0_635mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_56_7_5x18_5mm_p0_635mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_56_7_5x18_5mm_p0_635mm-B0A998.stl", convexity=10);
}

module kicad_ssop_8_2_95x2_8mm_p0_65mm() {
    color("#D2D1C7") import("kicad_ssop_8_2_95x2_8mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_8_2_95x2_8mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_8_2_95x2_8mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_ssop_8_3_95x5_21x3_27mm_p1_27mm() {
    color("#D2D1C7") import("kicad_ssop_8_3_95x5_21x3_27mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_8_3_95x5_21x3_27mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_8_3_95x5_21x3_27mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_ssop_8_3_9x5_05mm_p1_27mm() {
    color("#D2D1C7") import("kicad_ssop_8_3_9x5_05mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_8_3_9x5_05mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_8_3_9x5_05mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_ssop_8_5_25x5_24mm_p1_27mm() {
    color("#D2D1C7") import("kicad_ssop_8_5_25x5_24mm_p1_27mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_ssop_8_5_25x5_24mm_p1_27mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_ssop_8_5_25x5_24mm_p1_27mm-B0A998.stl", convexity=10);
}

module kicad_tssop_10_3x3mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_10_3x3mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_10_3x3mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_10_3x3mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_14_1ep_4_4x5mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_14_1ep_4_4x5mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_14_1ep_4_4x5mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_14_1ep_4_4x5mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_14_4_4x5mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_14_4_4x5mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_14_4_4x5mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_14_4_4x5mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_16_1ep_4_4x5mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_16_1ep_4_4x5mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_16_1ep_4_4x5mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_16_1ep_4_4x5mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_16_1ep_4_4x5mm_pitch0_65mm_ep3_4x5mm() {
    color("#D2D1C7") import("kicad_tssop_16_1ep_4_4x5mm_pitch0_65mm_ep3_4x5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_16_1ep_4_4x5mm_pitch0_65mm_ep3_4x5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_16_1ep_4_4x5mm_pitch0_65mm_ep3_4x5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_16_4_4x5mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_16_4_4x5mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_16_4_4x5mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_16_4_4x5mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_20_4_4x5mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_20_4_4x5mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_20_4_4x5mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_20_4_4x5mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_20_4_4x6_5mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_20_4_4x6_5mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_20_4_4x6_5mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_20_4_4x6_5mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_24_4_4x5mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_24_4_4x5mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_24_4_4x5mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_24_4_4x5mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_24_4_4x6_5mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_24_4_4x6_5mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_24_4_4x6_5mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_24_4_4x6_5mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_24_4_4x7_8mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_24_4_4x7_8mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_24_4_4x7_8mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_24_4_4x7_8mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_24_6_1x7_8mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_24_6_1x7_8mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_24_6_1x7_8mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_24_6_1x7_8mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_28_4_4x7_8mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_28_4_4x7_8mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_28_4_4x7_8mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_28_4_4x7_8mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_28_4_4x9_7mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_28_4_4x9_7mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_28_4_4x9_7mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_28_4_4x9_7mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_28_4_4x9_7mm_pitch0_65mm() {
    color("#D2D1C7") import("kicad_tssop_28_4_4x9_7mm_pitch0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_28_4_4x9_7mm_pitch0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_28_4_4x9_7mm_pitch0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_28_6_1x7_8mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_28_6_1x7_8mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_28_6_1x7_8mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_28_6_1x7_8mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_28_6_1x9_7mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_28_6_1x9_7mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_28_6_1x9_7mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_28_6_1x9_7mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_28_8x9_7mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_28_8x9_7mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_28_8x9_7mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_28_8x9_7mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_30_4_4x7_8mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_30_4_4x7_8mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_30_4_4x7_8mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_30_4_4x7_8mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_30_6_1x9_7mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_30_6_1x9_7mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_30_6_1x9_7mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_30_6_1x9_7mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_32_4_4x6_5mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_32_4_4x6_5mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_32_4_4x6_5mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_32_4_4x6_5mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_32_6_1x11mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_32_6_1x11mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_32_6_1x11mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_32_6_1x11mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_32_8x11mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_32_8x11mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_32_8x11mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_32_8x11mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_36_4_4x7_8mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_36_4_4x7_8mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_36_4_4x7_8mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_36_4_4x7_8mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_36_4_4x9_7mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_36_4_4x9_7mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_36_4_4x9_7mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_36_4_4x9_7mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_36_6_1x12_5mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_36_6_1x12_5mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_36_6_1x12_5mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_36_6_1x12_5mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_36_6_1x7_8mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_36_6_1x7_8mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_36_6_1x7_8mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_36_6_1x7_8mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_36_6_1x9_7mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_36_6_1x9_7mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_36_6_1x9_7mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_36_6_1x9_7mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_36_8x12_5mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_36_8x12_5mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_36_8x12_5mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_36_8x12_5mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_36_8x9_7mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_36_8x9_7mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_36_8x9_7mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_36_8x9_7mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_38_4_4x9_7mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_38_4_4x9_7mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_38_4_4x9_7mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_38_4_4x9_7mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_38_6_1x12_5mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_38_6_1x12_5mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_38_6_1x12_5mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_38_6_1x12_5mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_40_6_1x11mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_40_6_1x11mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_40_6_1x11mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_40_6_1x11mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_40_6_1x14mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_40_6_1x14mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_40_6_1x14mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_40_6_1x14mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_40_8x11mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_40_8x11mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_40_8x11mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_40_8x11mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_40_8x14mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_40_8x14mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_40_8x14mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_40_8x14mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_44_4_4x11_2mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_44_4_4x11_2mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_44_4_4x11_2mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_44_4_4x11_2mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_44_4_4x11mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_44_4_4x11mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_44_4_4x11mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_44_4_4x11mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_44_6_1x11mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_44_6_1x11mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_44_6_1x11mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_44_6_1x11mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_48_4_4x9_7mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_48_4_4x9_7mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_48_4_4x9_7mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_48_4_4x9_7mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_48_6_1x12_5mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_48_6_1x12_5mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_48_6_1x12_5mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_48_6_1x12_5mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_48_6_1x9_7mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_48_6_1x9_7mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_48_6_1x9_7mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_48_6_1x9_7mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_48_8x12_5mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_48_8x12_5mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_48_8x12_5mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_48_8x12_5mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_48_8x9_7mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_48_8x9_7mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_48_8x9_7mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_48_8x9_7mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_50_4_4x12_5mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_50_4_4x12_5mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_50_4_4x12_5mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_50_4_4x12_5mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_52_6_1x11mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_52_6_1x11mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_52_6_1x11mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_52_6_1x11mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_52_8x11mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_52_8x11mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_52_8x11mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_52_8x11mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_56_6_1x12_5mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_56_6_1x12_5mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_56_6_1x12_5mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_56_6_1x12_5mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_56_6_1x14mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_56_6_1x14mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_56_6_1x14mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_56_6_1x14mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_56_8x12_5mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_56_8x12_5mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_56_8x12_5mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_56_8x12_5mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_56_8x14mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_56_8x14mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_56_8x14mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_56_8x14mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_60_8x12_5mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_60_8x12_5mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_60_8x12_5mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_60_8x12_5mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_64_6_1x14mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_64_6_1x14mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_64_6_1x14mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_64_6_1x14mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_64_6_1x17mm_p0_5mm() {
    color("#D2D1C7") import("kicad_tssop_64_6_1x17mm_p0_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_64_6_1x17mm_p0_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_64_6_1x17mm_p0_5mm-B0A998.stl", convexity=10);
}

module kicad_tssop_64_8x14mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_64_8x14mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_64_8x14mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_64_8x14mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_68_8x14mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_68_8x14mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_68_8x14mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_68_8x14mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_80_6_1x17mm_p0_4mm() {
    color("#D2D1C7") import("kicad_tssop_80_6_1x17mm_p0_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_80_6_1x17mm_p0_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_80_6_1x17mm_p0_4mm-B0A998.stl", convexity=10);
}

module kicad_tssop_8_3x3mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_8_3x3mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_8_3x3mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_8_3x3mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_tssop_8_4_4x3mm_p0_65mm() {
    color("#D2D1C7") import("kicad_tssop_8_4_4x3mm_p0_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_tssop_8_4_4x3mm_p0_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_tssop_8_4_4x3mm_p0_65mm-B0A998.stl", convexity=10);
}

module kicad_dip_10_w10_16mm() {
    color("#252424") import("kicad_dip_10_w10_16mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_10_w10_16mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_10_w7_62mm() {
    color("#252424") import("kicad_dip_10_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_10_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_12_w10_16mm() {
    color("#252424") import("kicad_dip_12_w10_16mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_12_w10_16mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_12_w7_62mm() {
    color("#252424") import("kicad_dip_12_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_12_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_14_w10_16mm() {
    color("#252424") import("kicad_dip_14_w10_16mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_14_w10_16mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_14_w7_62mm() {
    color("#252424") import("kicad_dip_14_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_14_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_16_w10_16mm() {
    color("#252424") import("kicad_dip_16_w10_16mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_16_w10_16mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_16_w7_62mm() {
    color("#252424") import("kicad_dip_16_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_16_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_18_w7_62mm() {
    color("#252424") import("kicad_dip_18_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_18_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_20_w7_62mm() {
    color("#252424") import("kicad_dip_20_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_20_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_22_w10_16mm() {
    color("#252424") import("kicad_dip_22_w10_16mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_22_w10_16mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_22_w7_62mm() {
    color("#252424") import("kicad_dip_22_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_22_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_24_w10_16mm() {
    color("#252424") import("kicad_dip_24_w10_16mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_24_w10_16mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_24_w15_24mm() {
    color("#252424") import("kicad_dip_24_w15_24mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_24_w15_24mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_24_w7_62mm() {
    color("#252424") import("kicad_dip_24_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_24_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_28_w15_24mm() {
    color("#252424") import("kicad_dip_28_w15_24mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_28_w15_24mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_28_w7_62mm() {
    color("#252424") import("kicad_dip_28_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_28_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_32_w15_24mm() {
    color("#252424") import("kicad_dip_32_w15_24mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_32_w15_24mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_32_w7_62mm() {
    color("#252424") import("kicad_dip_32_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_32_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_40_w15_24mm() {
    color("#252424") import("kicad_dip_40_w15_24mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_40_w15_24mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_40_w25_4mm() {
    color("#252424") import("kicad_dip_40_w25_4mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_40_w25_4mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_42_w15_24mm() {
    color("#252424") import("kicad_dip_42_w15_24mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_42_w15_24mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_48_w15_24mm() {
    color("#252424") import("kicad_dip_48_w15_24mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_48_w15_24mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_4_w10_16mm() {
    color("#252424") import("kicad_dip_4_w10_16mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_4_w10_16mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_4_w7_62mm() {
    color("#252424") import("kicad_dip_4_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_4_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_5_6_w10_16mm() {
    color("#252424") import("kicad_dip_5_6_w10_16mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_5_6_w10_16mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_5_6_w7_62mm() {
    color("#252424") import("kicad_dip_5_6_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_5_6_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_64_w15_24mm() {
    color("#252424") import("kicad_dip_64_w15_24mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_64_w15_24mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_64_w22_86mm() {
    color("#252424") import("kicad_dip_64_w22_86mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_64_w22_86mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_64_w25_4mm() {
    color("#252424") import("kicad_dip_64_w25_4mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_64_w25_4mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_6_w10_16mm() {
    color("#252424") import("kicad_dip_6_w10_16mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_6_w10_16mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_6_w7_62mm() {
    color("#252424") import("kicad_dip_6_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_6_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_8_n6_w7_62mm() {
    color("#252424") import("kicad_dip_8_n6_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_8_n6_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_8_n7_w7_62mm() {
    color("#252424") import("kicad_dip_8_n7_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_8_n7_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_8_w10_16mm() {
    color("#252424") import("kicad_dip_8_w10_16mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_8_w10_16mm-D2D1C7.stl", convexity=10);
}

module kicad_dip_8_w7_62mm() {
    color("#252424") import("kicad_dip_8_w7_62mm-252424.stl", convexity=10);
    color("#D2D1C7") import("kicad_dip_8_w7_62mm-D2D1C7.stl", convexity=10);
}

module kicad_qfn_12_1ep_3x3mm_p0_5mm_ep1_65x1_65mm() {
    color("#D2D1C7") import("kicad_qfn_12_1ep_3x3mm_p0_5mm_ep1_65x1_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_12_1ep_3x3mm_p0_5mm_ep1_65x1_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_12_1ep_3x3mm_p0_5mm_ep1_65x1_65mm-B0A998.stl", convexity=10);
}

module kicad_qfn_16_1ep_3x3mm_p0_5mm_ep1_8x1_8mm() {
    color("#D2D1C7") import("kicad_qfn_16_1ep_3x3mm_p0_5mm_ep1_8x1_8mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_16_1ep_3x3mm_p0_5mm_ep1_8x1_8mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_16_1ep_3x3mm_p0_5mm_ep1_8x1_8mm-B0A998.stl", convexity=10);
}

module kicad_qfn_16_1ep_4x4mm_p0_65mm_ep2_1x2_1mm() {
    color("#D2D1C7") import("kicad_qfn_16_1ep_4x4mm_p0_65mm_ep2_1x2_1mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_16_1ep_4x4mm_p0_65mm_ep2_1x2_1mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_16_1ep_4x4mm_p0_65mm_ep2_1x2_1mm-B0A998.stl", convexity=10);
}

module kicad_qfn_16_1ep_5x5mm_p0_8mm_ep2_7x2_7mm() {
    color("#D2D1C7") import("kicad_qfn_16_1ep_5x5mm_p0_8mm_ep2_7x2_7mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_16_1ep_5x5mm_p0_8mm_ep2_7x2_7mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_16_1ep_5x5mm_p0_8mm_ep2_7x2_7mm-B0A998.stl", convexity=10);
}

module kicad_qfn_20_1ep_3x3mm_p0_45mm_ep1_6x1_6mm() {
    color("#D2D1C7") import("kicad_qfn_20_1ep_3x3mm_p0_45mm_ep1_6x1_6mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_20_1ep_3x3mm_p0_45mm_ep1_6x1_6mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_20_1ep_3x3mm_p0_45mm_ep1_6x1_6mm-B0A998.stl", convexity=10);
}

module kicad_qfn_20_1ep_3x4mm_p0_5mm_ep1_65x2_65mm() {
    color("#D2D1C7") import("kicad_qfn_20_1ep_3x4mm_p0_5mm_ep1_65x2_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_20_1ep_3x4mm_p0_5mm_ep1_65x2_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_20_1ep_3x4mm_p0_5mm_ep1_65x2_65mm-B0A998.stl", convexity=10);
}

module kicad_qfn_20_1ep_4x4mm_p0_5mm_ep2_25x2_25mm() {
    color("#D2D1C7") import("kicad_qfn_20_1ep_4x4mm_p0_5mm_ep2_25x2_25mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_20_1ep_4x4mm_p0_5mm_ep2_25x2_25mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_20_1ep_4x4mm_p0_5mm_ep2_25x2_25mm-B0A998.stl", convexity=10);
}

module kicad_qfn_20_1ep_4x5mm_p0_5mm_ep2_65x3_65mm() {
    color("#D2D1C7") import("kicad_qfn_20_1ep_4x5mm_p0_5mm_ep2_65x3_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_20_1ep_4x5mm_p0_5mm_ep2_65x3_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_20_1ep_4x5mm_p0_5mm_ep2_65x3_65mm-B0A998.stl", convexity=10);
}

module kicad_qfn_20_1ep_5x5mm_p0_65mm_ep3_35x3_35mm() {
    color("#D2D1C7") import("kicad_qfn_20_1ep_5x5mm_p0_65mm_ep3_35x3_35mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_20_1ep_5x5mm_p0_65mm_ep3_35x3_35mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_20_1ep_5x5mm_p0_65mm_ep3_35x3_35mm-B0A998.stl", convexity=10);
}

module kicad_qfn_24_1ep_3x3mm_p0_4mm_ep1_75x1_6mm() {
    color("#D2D1C7") import("kicad_qfn_24_1ep_3x3mm_p0_4mm_ep1_75x1_6mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_24_1ep_3x3mm_p0_4mm_ep1_75x1_6mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_24_1ep_3x3mm_p0_4mm_ep1_75x1_6mm-B0A998.stl", convexity=10);
}

module kicad_qfn_24_1ep_3x4mm_p0_4mm_ep1_65x2_65mm() {
    color("#D2D1C7") import("kicad_qfn_24_1ep_3x4mm_p0_4mm_ep1_65x2_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_24_1ep_3x4mm_p0_4mm_ep1_65x2_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_24_1ep_3x4mm_p0_4mm_ep1_65x2_65mm-B0A998.stl", convexity=10);
}

module kicad_qfn_24_1ep_4x4mm_p0_5mm_ep2_6x2_6mm() {
    color("#D2D1C7") import("kicad_qfn_24_1ep_4x4mm_p0_5mm_ep2_6x2_6mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_24_1ep_4x4mm_p0_5mm_ep2_6x2_6mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_24_1ep_4x4mm_p0_5mm_ep2_6x2_6mm-B0A998.stl", convexity=10);
}

module kicad_qfn_24_1ep_4x5mm_p0_5mm_ep2_65x3_65mm() {
    color("#D2D1C7") import("kicad_qfn_24_1ep_4x5mm_p0_5mm_ep2_65x3_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_24_1ep_4x5mm_p0_5mm_ep2_65x3_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_24_1ep_4x5mm_p0_5mm_ep2_65x3_65mm-B0A998.stl", convexity=10);
}

module kicad_qfn_24_1ep_5x5mm_p0_65mm_ep3_2x3_2mm() {
    color("#D2D1C7") import("kicad_qfn_24_1ep_5x5mm_p0_65mm_ep3_2x3_2mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_24_1ep_5x5mm_p0_65mm_ep3_2x3_2mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_24_1ep_5x5mm_p0_65mm_ep3_2x3_2mm-B0A998.stl", convexity=10);
}

module kicad_qfn_28_1ep_3x6mm_p0_5mm_ep1_7x4_75mm() {
    color("#D2D1C7") import("kicad_qfn_28_1ep_3x6mm_p0_5mm_ep1_7x4_75mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_28_1ep_3x6mm_p0_5mm_ep1_7x4_75mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_28_1ep_3x6mm_p0_5mm_ep1_7x4_75mm-B0A998.stl", convexity=10);
}

module kicad_qfn_28_1ep_4x4mm_p0_45mm_ep2_4x2_4mm() {
    color("#D2D1C7") import("kicad_qfn_28_1ep_4x4mm_p0_45mm_ep2_4x2_4mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_28_1ep_4x4mm_p0_45mm_ep2_4x2_4mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_28_1ep_4x4mm_p0_45mm_ep2_4x2_4mm-B0A998.stl", convexity=10);
}

module kicad_qfn_28_1ep_4x5mm_p0_5mm_ep2_65x3_25mm() {
    color("#D2D1C7") import("kicad_qfn_28_1ep_4x5mm_p0_5mm_ep2_65x3_25mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_28_1ep_4x5mm_p0_5mm_ep2_65x3_25mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_28_1ep_4x5mm_p0_5mm_ep2_65x3_25mm-B0A998.stl", convexity=10);
}

module kicad_qfn_28_1ep_5x5mm_p0_5mm_ep3_35x3_35mm() {
    color("#D2D1C7") import("kicad_qfn_28_1ep_5x5mm_p0_5mm_ep3_35x3_35mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_28_1ep_5x5mm_p0_5mm_ep3_35x3_35mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_28_1ep_5x5mm_p0_5mm_ep3_35x3_35mm-B0A998.stl", convexity=10);
}

module kicad_qfn_28_1ep_5x6mm_p0_5mm_ep3_65x4_65mm() {
    color("#D2D1C7") import("kicad_qfn_28_1ep_5x6mm_p0_5mm_ep3_65x4_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_28_1ep_5x6mm_p0_5mm_ep3_65x4_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_28_1ep_5x6mm_p0_5mm_ep3_65x4_65mm-B0A998.stl", convexity=10);
}

module kicad_qfn_28_1ep_6x6mm_p0_65mm_ep4_25x4_25mm() {
    color("#D2D1C7") import("kicad_qfn_28_1ep_6x6mm_p0_65mm_ep4_25x4_25mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_28_1ep_6x6mm_p0_65mm_ep4_25x4_25mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_28_1ep_6x6mm_p0_65mm_ep4_25x4_25mm-B0A998.stl", convexity=10);
}

module kicad_qfn_32_1ep_4x4mm_p0_4mm_ep2_65x2_65mm() {
    color("#D2D1C7") import("kicad_qfn_32_1ep_4x4mm_p0_4mm_ep2_65x2_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_32_1ep_4x4mm_p0_4mm_ep2_65x2_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_32_1ep_4x4mm_p0_4mm_ep2_65x2_65mm-B0A998.stl", convexity=10);
}

module kicad_qfn_32_1ep_5x5mm_p0_5mm_ep3_1x3_1mm() {
    color("#D2D1C7") import("kicad_qfn_32_1ep_5x5mm_p0_5mm_ep3_1x3_1mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_32_1ep_5x5mm_p0_5mm_ep3_1x3_1mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_32_1ep_5x5mm_p0_5mm_ep3_1x3_1mm-B0A998.stl", convexity=10);
}

module kicad_qfn_32_1ep_7x7mm_p0_65mm_ep4_65x4_65mm() {
    color("#D2D1C7") import("kicad_qfn_32_1ep_7x7mm_p0_65mm_ep4_65x4_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_32_1ep_7x7mm_p0_65mm_ep4_65x4_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_32_1ep_7x7mm_p0_65mm_ep4_65x4_65mm-B0A998.stl", convexity=10);
}

module kicad_qfn_36_1ep_5x6mm_p0_5mm_ep3_6x4_1mm() {
    color("#D2D1C7") import("kicad_qfn_36_1ep_5x6mm_p0_5mm_ep3_6x4_1mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_36_1ep_5x6mm_p0_5mm_ep3_6x4_1mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_36_1ep_5x6mm_p0_5mm_ep3_6x4_1mm-B0A998.stl", convexity=10);
}

module kicad_qfn_36_1ep_6x6mm_p0_5mm_ep3_7x3_7mm() {
    color("#D2D1C7") import("kicad_qfn_36_1ep_6x6mm_p0_5mm_ep3_7x3_7mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_36_1ep_6x6mm_p0_5mm_ep3_7x3_7mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_36_1ep_6x6mm_p0_5mm_ep3_7x3_7mm-B0A998.stl", convexity=10);
}

module kicad_qfn_38_1ep_4x6mm_p0_4mm_ep2_65x4_65mm() {
    color("#D2D1C7") import("kicad_qfn_38_1ep_4x6mm_p0_4mm_ep2_65x4_65mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_38_1ep_4x6mm_p0_4mm_ep2_65x4_65mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_38_1ep_4x6mm_p0_4mm_ep2_65x4_65mm-B0A998.stl", convexity=10);
}

module kicad_qfn_38_1ep_5x7mm_p0_5mm_ep3_15x5_15mm() {
    color("#D2D1C7") import("kicad_qfn_38_1ep_5x7mm_p0_5mm_ep3_15x5_15mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_38_1ep_5x7mm_p0_5mm_ep3_15x5_15mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_38_1ep_5x7mm_p0_5mm_ep3_15x5_15mm-B0A998.stl", convexity=10);
}

module kicad_qfn_40_1ep_5x5mm_p0_4mm_ep3_6x3_6mm() {
    color("#D2D1C7") import("kicad_qfn_40_1ep_5x5mm_p0_4mm_ep3_6x3_6mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_40_1ep_5x5mm_p0_4mm_ep3_6x3_6mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_40_1ep_5x5mm_p0_4mm_ep3_6x3_6mm-B0A998.stl", convexity=10);
}

module kicad_qfn_40_1ep_6x6mm_p0_5mm_ep4_6x4_6mm() {
    color("#D2D1C7") import("kicad_qfn_40_1ep_6x6mm_p0_5mm_ep4_6x4_6mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_40_1ep_6x6mm_p0_5mm_ep4_6x4_6mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_40_1ep_6x6mm_p0_5mm_ep4_6x4_6mm-B0A998.stl", convexity=10);
}

module kicad_qfn_42_1ep_5x6mm_p0_4mm_ep3_7x4_7mm() {
    color("#D2D1C7") import("kicad_qfn_42_1ep_5x6mm_p0_4mm_ep3_7x4_7mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_42_1ep_5x6mm_p0_4mm_ep3_7x4_7mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_42_1ep_5x6mm_p0_4mm_ep3_7x4_7mm-B0A998.stl", convexity=10);
}

module kicad_qfn_44_1ep_7x7mm_p0_5mm_ep5_15x5_15mm() {
    color("#D2D1C7") import("kicad_qfn_44_1ep_7x7mm_p0_5mm_ep5_15x5_15mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_44_1ep_7x7mm_p0_5mm_ep5_15x5_15mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_44_1ep_7x7mm_p0_5mm_ep5_15x5_15mm-B0A998.stl", convexity=10);
}

module kicad_qfn_44_1ep_8x8mm_p0_65mm_ep6_45x6_45mm() {
    color("#D2D1C7") import("kicad_qfn_44_1ep_8x8mm_p0_65mm_ep6_45x6_45mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_44_1ep_8x8mm_p0_65mm_ep6_45x6_45mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_44_1ep_8x8mm_p0_65mm_ep6_45x6_45mm-B0A998.stl", convexity=10);
}

module kicad_qfn_44_1ep_9x9mm_p0_65mm_ep7_5x7_5mm() {
    color("#D2D1C7") import("kicad_qfn_44_1ep_9x9mm_p0_65mm_ep7_5x7_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_44_1ep_9x9mm_p0_65mm_ep7_5x7_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_44_1ep_9x9mm_p0_65mm_ep7_5x7_5mm-B0A998.stl", convexity=10);
}

module kicad_qfn_48_1ep_5x5mm_p0_35mm_ep3_7x3_7mm() {
    color("#D2D1C7") import("kicad_qfn_48_1ep_5x5mm_p0_35mm_ep3_7x3_7mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_48_1ep_5x5mm_p0_35mm_ep3_7x3_7mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_48_1ep_5x5mm_p0_35mm_ep3_7x3_7mm-B0A998.stl", convexity=10);
}

module kicad_qfn_48_1ep_6x6mm_p0_4mm_ep4_3x4_3mm() {
    color("#D2D1C7") import("kicad_qfn_48_1ep_6x6mm_p0_4mm_ep4_3x4_3mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_48_1ep_6x6mm_p0_4mm_ep4_3x4_3mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_48_1ep_6x6mm_p0_4mm_ep4_3x4_3mm-B0A998.stl", convexity=10);
}

module kicad_qfn_48_1ep_7x7mm_p0_5mm_ep5_15x5_15mm() {
    color("#D2D1C7") import("kicad_qfn_48_1ep_7x7mm_p0_5mm_ep5_15x5_15mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_48_1ep_7x7mm_p0_5mm_ep5_15x5_15mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_48_1ep_7x7mm_p0_5mm_ep5_15x5_15mm-B0A998.stl", convexity=10);
}

module kicad_qfn_52_1ep_7x8mm_p0_5mm_ep5_41x6_45mm() {
    color("#D2D1C7") import("kicad_qfn_52_1ep_7x8mm_p0_5mm_ep5_41x6_45mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_52_1ep_7x8mm_p0_5mm_ep5_41x6_45mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_52_1ep_7x8mm_p0_5mm_ep5_41x6_45mm-B0A998.stl", convexity=10);
}

module kicad_qfn_56_1ep_7x7mm_p0_4mm_ep3_2x3_2mm() {
    color("#D2D1C7") import("kicad_qfn_56_1ep_7x7mm_p0_4mm_ep3_2x3_2mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_56_1ep_7x7mm_p0_4mm_ep3_2x3_2mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_56_1ep_7x7mm_p0_4mm_ep3_2x3_2mm-B0A998.stl", convexity=10);
}

module kicad_qfn_56_1ep_8x8mm_p0_5mm_ep4_5x5_2mm() {
    color("#D2D1C7") import("kicad_qfn_56_1ep_8x8mm_p0_5mm_ep4_5x5_2mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_56_1ep_8x8mm_p0_5mm_ep4_5x5_2mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_56_1ep_8x8mm_p0_5mm_ep4_5x5_2mm-B0A998.stl", convexity=10);
}

module kicad_qfn_64_1ep_8x8mm_p0_4mm_ep6_5x6_5mm() {
    color("#D2D1C7") import("kicad_qfn_64_1ep_8x8mm_p0_4mm_ep6_5x6_5mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_64_1ep_8x8mm_p0_4mm_ep6_5x6_5mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_64_1ep_8x8mm_p0_4mm_ep6_5x6_5mm-B0A998.stl", convexity=10);
}

module kicad_qfn_64_1ep_9x9mm_p0_5mm_ep4_7x4_7mm() {
    color("#D2D1C7") import("kicad_qfn_64_1ep_9x9mm_p0_5mm_ep4_7x4_7mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_64_1ep_9x9mm_p0_5mm_ep4_7x4_7mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_64_1ep_9x9mm_p0_5mm_ep4_7x4_7mm-B0A998.stl", convexity=10);
}

module kicad_qfn_68_1ep_8x8mm_p0_4mm_ep5_2x5_2mm() {
    color("#D2D1C7") import("kicad_qfn_68_1ep_8x8mm_p0_4mm_ep5_2x5_2mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_68_1ep_8x8mm_p0_4mm_ep5_2x5_2mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_68_1ep_8x8mm_p0_4mm_ep5_2x5_2mm-B0A998.stl", convexity=10);
}

module kicad_qfn_76_1ep_9x9mm_p0_4mm_ep3_8x3_8mm() {
    color("#D2D1C7") import("kicad_qfn_76_1ep_9x9mm_p0_4mm_ep3_8x3_8mm-D2D1C7.stl", convexity=10);
    color("#252424") import("kicad_qfn_76_1ep_9x9mm_p0_4mm_ep3_8x3_8mm-252424.stl", convexity=10);
    color("#B0A998") import("kicad_qfn_76_1ep_9x9mm_p0_4mm_ep3_8x3_8mm-B0A998.stl", convexity=10);
}

