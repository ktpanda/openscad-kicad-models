#!/bin/sh
IFS=

mydir=`cd \`dirname $0\`; pwd`

wrlpath=$1

[ -z "$wrlpath" ] && wrlpath=/usr/share/kicad/3dmodels

cd $wrlpath

$mydir/wrl2scad.py --prefix='kicad_' --lc -o $mydir/wrl-models/kicad-models-wrl.scad \
    Package_TO_SOT_SMD.3dshapes/SOT-23.wrl \
    Package_TO_SOT_SMD.3dshapes/SOT-223.wrl \
    Button_Switch_SMD.3dshapes/SW_SPST_B3U-1000P.wrl \
    Buzzer_Beeper.3dshapes/MagneticBuzzer_ProSignal_ABI-010-RC.wrl \
    Connector_HDMI.3dshapes/HDMI_A_Molex_208658-1001_Horizontal.wrl \
    Connector_USB.3dshapes/USB_Micro-B_Molex_47346-0001.wrl\
    Connector_USB.3dshapes/USB_C_*.wrl\
    Resistor_SMD.3dshapes/R_[0-9]???_*.wrl \
    Diode_SMD.3dshapes/D_[0-9]???_*.wrl \
    Capacitor_SMD.3dshapes/C_[0-9]???_*.wrl \
    LED_SMD.3dshapes/LED_[0-9]???_????Metric.wrl \
    LED_SMD.3dshapes/LED_RGB_5050-6.wrl \
    RF_Module.3dshapes/ESP-12E.wrl \
    Button_Switch_THT.3dshapes/SW_PUSH_6mm.wrl \
    Package_SO.3dshapes/SOIC-*.wrl  \
    Package_SO.3dshapes/SSOP-*.wrl  \
    Package_SO.3dshapes/TSSOP-*.wrl  \
    Package_DIP.3dshapes/DIP-*mm.wrl \
    Package_DFN_QFN.3dshapes/QFN-12-1EP_3x3mm_P0.5mm_EP1.65x1.65mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-16-1EP_3x3mm_P0.5mm_EP1.8x1.8mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-16-1EP_4x4mm_P0.65mm_EP2.1x2.1mm.wrl   \
    Package_DFN_QFN.3dshapes/QFN-16-1EP_5x5mm_P0.8mm_EP2.7x2.7mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-20-1EP_3x3mm_P0.45mm_EP1.6x1.6mm.wrl   \
    Package_DFN_QFN.3dshapes/QFN-20-1EP_3x4mm_P0.5mm_EP1.65x2.65mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-20-1EP_4x4mm_P0.5mm_EP2.25x2.25mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-20-1EP_4x5mm_P0.5mm_EP2.65x3.65mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-20-1EP_5x5mm_P0.65mm_EP3.35x3.35mm.wrl \
    Package_DFN_QFN.3dshapes/QFN-24-1EP_3x3mm_P0.4mm_EP1.75x1.6mm.wrl   \
    Package_DFN_QFN.3dshapes/QFN-24-1EP_3x4mm_P0.4mm_EP1.65x2.65mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-24-1EP_4x4mm_P0.5mm_EP2.6x2.6mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-24-1EP_4x5mm_P0.5mm_EP2.65x3.65mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-24-1EP_5x5mm_P0.65mm_EP3.2x3.2mm.wrl   \
    Package_DFN_QFN.3dshapes/QFN-28-1EP_3x6mm_P0.5mm_EP1.7x4.75mm.wrl   \
    Package_DFN_QFN.3dshapes/QFN-28-1EP_4x4mm_P0.45mm_EP2.4x2.4mm.wrl   \
    Package_DFN_QFN.3dshapes/QFN-28-1EP_4x5mm_P0.5mm_EP2.65x3.25mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-28-1EP_5x5mm_P0.5mm_EP3.35x3.35mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-28-1EP_5x6mm_P0.5mm_EP3.65x4.65mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-28-1EP_6x6mm_P0.65mm_EP4.25x4.25mm.wrl \
    Package_DFN_QFN.3dshapes/QFN-32-1EP_4x4mm_P0.4mm_EP2.65x2.65mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-32-1EP_5x5mm_P0.5mm_EP3.1x3.1mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-32-1EP_7x7mm_P0.65mm_EP4.65x4.65mm.wrl \
    Package_DFN_QFN.3dshapes/QFN-36-1EP_5x6mm_P0.5mm_EP3.6x4.1mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-36-1EP_6x6mm_P0.5mm_EP3.7x3.7mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-38-1EP_4x6mm_P0.4mm_EP2.65x4.65mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-38-1EP_5x7mm_P0.5mm_EP3.15x5.15mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-40-1EP_5x5mm_P0.4mm_EP3.6x3.6mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-40-1EP_6x6mm_P0.5mm_EP4.6x4.6mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-42-1EP_5x6mm_P0.4mm_EP3.7x4.7mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-44-1EP_7x7mm_P0.5mm_EP5.15x5.15mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-44-1EP_8x8mm_P0.65mm_EP6.45x6.45mm.wrl \
    Package_DFN_QFN.3dshapes/QFN-44-1EP_9x9mm_P0.65mm_EP7.5x7.5mm.wrl   \
    Package_DFN_QFN.3dshapes/QFN-48-1EP_5x5mm_P0.35mm_EP3.7x3.7mm.wrl   \
    Package_DFN_QFN.3dshapes/QFN-48-1EP_6x6mm_P0.4mm_EP4.3x4.3mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-48-1EP_7x7mm_P0.5mm_EP5.15x5.15mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-52-1EP_7x8mm_P0.5mm_EP5.41x6.45mm.wrl  \
    Package_DFN_QFN.3dshapes/QFN-56-1EP_7x7mm_P0.4mm_EP3.2x3.2mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-56-1EP_8x8mm_P0.5mm_EP4.5x5.2mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-64-1EP_8x8mm_P0.4mm_EP6.5x6.5mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-64-1EP_9x9mm_P0.5mm_EP4.7x4.7mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-68-1EP_8x8mm_P0.4mm_EP5.2x5.2mm.wrl    \
    Package_DFN_QFN.3dshapes/QFN-76-1EP_9x9mm_P0.4mm_EP3.8x3.8mm.wrl    \
