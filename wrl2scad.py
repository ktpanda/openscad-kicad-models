#!/usr/bin/python3

import sys
import re
import time
import os
import argparse
import subprocess
import traceback
import multiprocessing
import math
import struct
from pathlib import Path
from collections import defaultdict

import ir
import wrlparser
from ir import Shape, IndexedFaceSet

from kicad_model import interpret_package

CHARS_INITIAL = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_'
CHARS_SEQ = CHARS_INITIAL + '0123456789'
KEYWORDS = {'if', 'ln'}

STL_HEADER = struct.Struct('<80sI')
STL_TRI = struct.Struct('<12fh')

PKG_ARGS = {
    'pin_header_vertical': ('r', 'n'),
    'pin_header_horizontal': ('r', 'n'),
    'pin_socket_vertical': ('r', 'n'),
    'electrolytic_capacitor': ('w', 'h'),
    'screw_terminal': ('n', 'p'),
    'spade_terminal': ('size', 'r', 'n', 'spc'),
    'dip': ('pins', 'w'),
    'qfn': ('pins', 'w', 'h'),
    'soic': ('pins', 'wide', 'w', 'h', 'spc'),
    'ssop': ('pins', 'w', 'h', 'spc'),
    'tssop': ('pins', 'w', 'h', 'spc'),
}

def hexcolor(rgb):
    return ''.join(format(min(255, max(0, int(v * 255))), '02X') for v in rgb)

def mknormal(va, vb, vc):
    x1 = vb[0] - va[0]
    y1 = vb[1] - va[1]
    z1 = vb[2] - va[2]

    x2 = vc[0] - va[0]
    y2 = vc[1] - va[1]
    z2 = vc[2] - va[2]

    cx = y1 * z2 - z1 * y2
    cy = z1 * x2 - x1 * z2
    cz = x1 * y2 - y1 * x2

    lensqr = cx*cx + cy*cy + cz*cz
    if lensqr > 0:
        cof = lensqr ** -0.5
        cx *= cof
        cy *= cof
        cz *= cof

    return cx, cy, cz

def write_stl(fp, hdr, tridata):
    fp.write(STL_HEADER.pack(hdr.encode('utf8'), len(tridata)))
    for norm, v1, v2, v3, attr in tridata:
        data = norm + v1 + v2 + v3 + (attr,)
        fp.write(STL_TRI.pack(*data))

def appearance_color(app):
    if (m := app.material) is None:
        return '888888'
    if (c := m.diffuseColor) is not None:
        return hexcolor(c)
    if (c := m.specularColor) is not None:
        return hexcolor(c)
    if (c := m.emissiveColor) is not None:
        return hexcolor(c)

def bump_var(idxlist):
    bump_index = len(idxlist) - 1
    while bump_index >= 0:
        v = idxlist[bump_index] + 1
        if v >= (len(CHARS_INITIAL) if bump_index == 0 else len(CHARS_SEQ)):
            idxlist[bump_index] = 0
            bump_index -= 1
        else:
            idxlist[bump_index] = v
            return
    idxlist.append(0)

def compact_num(n):
    '''Rounds `n` to 4 decimal places, and returns the most compact decimal
    representation of that number'''
    if n < 0:
        return '-' + compact_num(-n)
    return f'{n:.4f}'.rstrip('0').rstrip('.').lstrip('0') or '0'

def parse_wrl(args):
    path, mname, stldir = args

    print(f'Processing {path}', file=sys.stderr)

    bycolor = defaultdict(list)
    scene = wrlparser.parse(path.read_text(encoding='utf8'))

    # Counts how many times each exact value is used in the model. Used to determine if
    # saving it in a variable is more compact.
    value_count = defaultdict(lambda: 0)

    for node in scene.nodes:
        if isinstance(node, Shape) and node.geometry is not None:
            clr = appearance_color(node.appearance)
            index = [[int(v) for v in lst] for lst in node.geometry.coordIndex]

            coords = [tuple([(x * 2.54) for x in pt]) for pt in node.geometry.coord.point]
            #for pt in coords:
            #    for c in pt:
            #        value_count[c] += 1
            bycolor[clr].append((coords, index))

    text = ['    ']

    vars = {}
    idxlist = [0]
    curvar = 'a'

    # Remove any whitespace at the end of variable definitions
    while text and (text[-1].strip() == ''):
        text.pop()

    # If we had any definitions, add a newline
    if text:
        text.append('\n')

    for clr, lst in bycolor.items():
        stlfile = f'{mname}-{clr}.stl'
        text.append(f'    color("#{clr}") import("{stlfile}", convexity=10);\n')

        tris = []

        for coords, index in lst:

            for tri in index:
                idxa, idxb, idxc = tri
                va = coords[idxa]
                vb = coords[idxb]
                vc = coords[idxc]
                norm = mknormal(va, vb, vc)
                tris.append((norm, va, vb, vc, 0))

        with (stldir / stlfile).open('wb') as fp:
            write_stl(fp, 'wrl2scad poly', tris)

    return path, mname, ''.join(text)

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument('wrl', nargs='*', type=Path, help='files')
    p.add_argument('-n', '--name', help='override module name')
    p.add_argument('-p', '--prefix', default='', help='prefix for module names')
    p.add_argument('-l', '--lc', action='store_true', help='lowercase module name')
    p.add_argument('-i', '--invoke', action='store_true', help='call module')
    p.add_argument('-o', '--output', type=Path, help='output file')
    #p.add_argument('-v', '--verbose', action='store_true', help='')
    #p.add_argument(help='')
    args = p.parse_args()

    #curvar = []
    #for j in range(100000):
    #    print(f'{j} {bump_var(curvar)}', file=sys.stderr)

    ir.console.print = lambda *a, **kw: None

    if args.output:
        out = args.output.open('w')
        stldir = args.output.parent
        pkgout = sys.stdout
    else:
        out = sys.stdout
        stldir = '.'
        pkgout = sys.stderr

    package = defaultdict(list)

    inputs = []

    for fil in args.wrl:
        pkg, pargs = interpret_package(fil.name)
        mname = args.name
        if mname is None:
            mname = fil.stem

        mname = re.sub(r'[^a-zA-Z0-9]+', '_', mname).strip('_')

        if args.lc:
            mname = mname.lower()

        mname = args.prefix + mname

        if pkg in PKG_ARGS:
            package[pkg].append((pargs, mname))

        inputs.append((fil, mname, stldir))

    with multiprocessing.Pool(8) as pool:
        for fil, mname, mtext in pool.imap(parse_wrl, inputs):
            print(f'module {mname}() {{', file=out)
            out.write(mtext)
            print(f'}}', file=out)
            if args.invoke:
                print(f'{mname}();', file=out)
            print('', file=out)

    for pkg, arglist in package.items():
        names = PKG_ARGS[pkg]
        print(f'module {pkg}({", ".join(names)}) {{')
        ifl = 'if'
        for argl, mname in arglist:
            test = " && ".join(f'{n} == {v}' for n, v in zip(names, argl))
            print(f'    {ifl}({test}) {{ {mname}(); }}')
            ifl = 'else if'
        print(f'}}')

    for pkg, arglist in package.items():
        names = PKG_ARGS[pkg]
        args = ', '.join(f'{nm}=$cargs[{i}]' for i, nm in enumerate(names))
        print(f'    else if ($cpackage == "{pkg}") {{ {pkg}({args}); }}')

if __name__ == '__main__':
    main()
