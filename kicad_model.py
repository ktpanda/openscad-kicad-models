#!/usr/bin/python3

import sys
import re
import time
import os
import argparse
import subprocess
import traceback
import tempfile
import shutil
import csv
from decimal import Decimal as D
from collections import defaultdict
from pathlib import Path
from hashlib import sha256

def kicad_export(path, type, output, *args):
    kicad_args = ['kicad-cli', 'pcb', 'export', type, path, '--output', output]
    kicad_args.extend(args)
    return subprocess.Popen(kicad_args)

module_base = '''
module %(name)s_pcb_layer(color, opacity, fname, z, thick, convexity, min_hole_size, hole_size_offset, stl=false) {
    color(color, opacity)
    if (stl) {
        import(str(fname, ".stl"));
    } else {
        translate([0, 0, z])
        linear_extrude(thick)
        render(convexity)
        difference() {
            translate(-%(name)s_origin) import(str(fname, ".dxf"));
            pcb_drill(%(name)s_drill, min_hole_size, hole_size_offset);
        }
    }

}

module %(name)s_pcb_model(traces=false, thick=1.6, trace_thick=0.05, convexity=10, pcb_color="#00bb00", trace_color="#00cc00", mask_color="#cccccc", silk_color="#f8f8f8", min_hole_size=0, hole_size_offset=0, stl=false) {
    color(pcb_color)
    if (stl) {
        import("%(name)s-pcb-board.stl", convexity=convexity);
    } else {
        linear_extrude(thick)
        render(convexity)
        difference() {
            translate(-%(name)s_origin) import("%(fname)s-pcb-board.dxf");
            pcb_drill(%(name)s_drill, min_hole_size, hole_size_offset);
        }
    }

    if (traces) {
        mask_thick = trace_thick + .01;
        silk_thick = mask_thick + 0.02;
        %(name)s_pcb_layer(trace_color, 0.5, "%(fname)s-pcb-trace-b", -trace_thick-0.001, trace_thick, convexity, min_hole_size, hole_size_offset, stl);
        %(name)s_pcb_layer(trace_color, 0.5, "%(fname)s-pcb-trace-f", thick+0.001, trace_thick, convexity, min_hole_size, hole_size_offset, stl);
        %(name)s_pcb_layer(mask_color, 1.0, "%(fname)s-pcb-mask-b", -mask_thick-0.001, mask_thick, convexity, min_hole_size, hole_size_offset, stl);
        %(name)s_pcb_layer(mask_color, 1.0, "%(fname)s-pcb-mask-f", thick+0.001, mask_thick, convexity, min_hole_size, hole_size_offset, stl);
        //%(name)s_pcb_layer(silk_color, 1.0, "%(fname)s-pcb-silk-b", -silk_thick-0.001, silk_thick, convexity, min_hole_size, hole_size_offset, stl);
        //%(name)s_pcb_layer(silk_color, 1.0, "%(fname)s-pcb-silk-f", thick+0.001, silk_thick, convexity, min_hole_size, hole_size_offset, stl);
    }
}

module %(name)s_pcb_model_part(part, thick=1.6, trace_thick=0.05, min_hole_size=0, hole_size_offset=0) {
    mask_thick = trace_thick + .01;
    if (part == "board") {
        linear_extrude(thick)
        difference() {
            translate(-%(name)s_origin) import("%(fname)s-pcb-board.dxf");
            pcb_drill(%(name)s_drill, 0, 0);
        }
    } else if (part == "trace-b") {
        %(name)s_pcb_layer("#666666", 0.5, "%(fname)s-pcb-trace-b", -trace_thick-0.001, trace_thick, 10, 0, 0, false);
    } else if (part == "trace-f") {
        %(name)s_pcb_layer("#666666", 0.5, "%(fname)s-pcb-trace-f", thick+0.001, trace_thick, 10, 0, 0, false);
    } else if (part == "mask-b") {
        %(name)s_pcb_layer("#666666", 1.0, "%(fname)s-pcb-mask-b", -mask_thick-0.001, mask_thick, 10, 0, 0, false);
    } else if (part == "mask-f") {
        %(name)s_pcb_layer("#666666", 1.0, "%(fname)s-pcb-mask-f", thick+0.001, mask_thick, 10, 0, 0, false);
    }
}
'''

def interpret_package(pkg):
    if m := re.match(r'Pin(Header|Socket)_(\d)x(\d\d)_P2.54mm_(Vertical|Horizontal)', pkg):
        return f'pin_{m.group(1).lower()}_{m.group(4).lower()}', [int(m.group(2)), int(m.group(3))]
    if m := re.match(r'CP_Radial_D(\d*\.?\d+)mm_P(\d*\.?\d+)mm', pkg):
        return f'electrolytic_capacitor', [m.group(1), m.group(2)]
    if m := re.match(r'TerminalBlock_bornier-(\d+)_P(\d*\.?\d+)mm', pkg):
        return f'screw_terminal', [m.group(1), m.group(2)]
    if m := re.match(r'Spade_Terminal_(\d*\.?\d+)mm_(\d+)x(\d+)_(\d*\.?\d+)mm', pkg):
        return f'spade_terminal', [m.group(1), int(m.group(2)), int(m.group(3)), m.group(4)]
    if m := re.match(r'Spade_Terminal_(\d*\.?\d+)mm', pkg):
        return f'spade_terminal', [m.group(1), 1, 1, 0]
    if m := re.match(r'DIP-(\d+)_W(\d*\.?\d+)mm', pkg):
        return f'dip', [m.group(1), m.group(2)]
    if m := re.match(r'QFN-(\d+)(?:-1EP)?_(\d+)x(\d+)mm', pkg):
        return f'qfn', [m.group(1), m.group(2), m.group(3)]
    if m := re.match(r'SOIC-(\d*\.?\d+)(W?)_(\d*\.?\d+)x(\d*\.?\d+)mm_P(\d*\.?\d+)mm', pkg):
        return f'soic', [m.group(1), 1 if m.group(2) == "W" else 0, m.group(3), m.group(4), m.group(5)]
    if m := re.match(r'SSOP-(\d*\.?\d+)_(\d*\.?\d+)x(\d*\.?\d+)mm_P(\d*\.?\d+)mm', pkg):
        return f'ssop', [m.group(1), m.group(2), m.group(3), m.group(4)]
    if m := re.match(r'TSSOP-(\d*\.?\d+)_(\d*\.?\d+)x(\d*\.?\d+)mm_P(\d*\.?\d+)mm', pkg):
        return f'tssop', [m.group(1), m.group(2), m.group(3), m.group(4)]


    if m := re.match(r"(D|LED|R|C)_(\d{4})_\d{4}Metric_Pad(\d*\.?\d+)x(\d*\.?\d+)mm(?:_HandSolder)?", pkg):
        comp = m.group(1).lower()
        size = m.group(2)
        w = m.group(3)
        h = m.group(4)
        return f'smd_{comp}', [f'"{size}"', w, h]

    if m := re.match(r"(D|LED|R|C)_(\d{4})_\d{4}Metric(?:_HandSolder)?", pkg):
        comp = m.group(1).lower()
        size = m.group(2)
        return f'smd_{comp}', [f'"{size}"', 0, 0]

    return pkg, []

def main():
    p = argparse.ArgumentParser(description='')
    p.add_argument('pcb', type=Path, help='kicad pcb file')
    p.add_argument('-n', '--name', help='base name of pcb module')
    p.add_argument('-o', '--outdir', type=Path, default=Path('.'), help='path to output files')
    p.add_argument('-s', '--stl', action='store_true',help='render to STL with default')

    #p.add_argument('-v', '--verbose', action='store_true', help='')
    #p.add_argument(help='')
    args = p.parse_args()
    fname = args.name

    if not fname:
        fname = args.pcb.stem

    name = fname.replace('-', '_')

    with args.pcb.open('rb') as fp:
        h = sha256()
        while (data := fp.read(2048)):
            h.update(data)
        pcb_hash = h.hexdigest()


    with tempfile.TemporaryDirectory(suffix='.kicad_model') as td:
        tempd = Path(td)
        procs = []
        pos_fil = tempd / 'pos.csv'
        board_dxf = args.outdir / f'{fname}-pcb-board.dxf'
        procs.append(kicad_export(args.pcb, 'dxf', board_dxf, '--ou', 'mm', '-l', 'Edge.Cuts'))
        procs.append(kicad_export(args.pcb, 'dxf', args.outdir / f'{fname}-pcb-trace-b.dxf', '--ou', 'mm',  '-l', 'B.Cu'))
        procs.append(kicad_export(args.pcb, 'dxf', args.outdir / f'{fname}-pcb-trace-f.dxf', '--ou', 'mm',  '-l', 'F.Cu'))
        procs.append(kicad_export(args.pcb, 'dxf', args.outdir / f'{fname}-pcb-mask-b.dxf', '--ou', 'mm',  '-l', 'B.Mask'))
        procs.append(kicad_export(args.pcb, 'dxf', args.outdir / f'{fname}-pcb-mask-f.dxf', '--ou', 'mm',  '-l', 'F.Mask'))
        #procs.append(kicad_export(args.pcb, 'dxf', args.outdir / f'{fname}-pcb-silk-b.dxf', '--ou', 'mm',  '-l', 'B.Silkscreen'))
        #procs.append(kicad_export(args.pcb, 'dxf', args.outdir / f'{fname}-pcb-silk-f.dxf', '--ou', 'mm',  '-l', 'F.Silkscreen'))
        procs.append(kicad_export(args.pcb, 'drill', f'{td}/'))
        procs.append(kicad_export(args.pcb, 'pos', pos_fil, '--format', 'csv', '--units', 'mm'))
        for proc in procs:
            proc.wait()

        for proc in procs:
            if proc.returncode != 0:
                return

        minx = None
        miny = None
        maxx = None
        maxy = None
        with board_dxf.open('r') as fp:
            for grpcode in fp:
                grpcode = grpcode.strip()
                data = next(fp, '')
                if grpcode in {'10', '11'}:
                    val = D(data)
                    if minx is None or val < minx:
                        minx = val
                    if maxx is None or val > maxx:
                        maxx = val
                elif grpcode in {'20', '21'}:
                    val = D(data)
                    if miny is None or val < miny:
                        miny = val
                    if maxy is None or val > maxy:
                        maxy = val

        minx = D('0') if minx is None else minx.normalize()
        miny = D('0') if miny is None else miny.normalize()
        maxx = D('0') if maxx is None else maxx.normalize()
        maxy = D('0') if maxy is None else maxy.normalize()

        files = [p for p in tempd.iterdir() if p.suffix == '.drl']
        if not files:
            print('Error: no drill files created', file=sys.stderr)
            return

        for f in files:
            plated = False
            tool_size = {}
            current_tool = None
            holes = defaultdict(list)

            with f.open('r') as fp:
                for line in fp:
                    if 'G85X' in line and (m := re.match(r'X([\-\+]?[\d\.]+)Y([\-\+]?[\d\.]+)G85X([\-\+]?[\d\.]+)Y([\-\+]?[\d\.]+)', line)):
                        holes[current_tool, True].append((
                            D(m.group(1)).normalize(),
                            D(m.group(2)).normalize(),
                            D(m.group(3)).normalize(),
                            D(m.group(4)).normalize(),
                        ))
                    elif m := re.match(r'X([\-\+]?[\d\.]+)Y([\-\+]?[\d\.]+)', line):
                        holes[current_tool, False].append((D(m.group(1)).normalize(), D(m.group(2)).normalize()))
                    elif m := re.match(r'T(\d+)C([\d\.]+)', line):
                        tool_size[int(m.group(1))] = D(m.group(2)).normalize(), plated
                    elif m := re.match(r'T(\d+)', line):
                        current_tool = tool_size.get(int(m.group(1)))
                    elif 'TA.AperFunction,Plated' in line:
                        plated = True
                    elif 'TA.AperFunction,NonPlated' in line:
                        plated = False

        objects = []
        with pos_fil.open('r', newline='') as fp:
            rdr = csv.DictReader(fp)
            for row in rdr:
                objects.append((
                    row['Ref'],
                    row['Side'],
                    (D(row['PosX']) - minx).normalize(),
                    (D(row['PosY']) - miny).normalize(),
                    D(row['Rot']).normalize(),
                    row['Val'],
                    row['Package']
                ))

        objects.sort(key=lambda v: (v[1], v[2], v[3]))

        packages = set()

        lines = []

        timestamp = time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())
        lines.append(f'// Generated by kicad_model.py from {args.pcb.name} SHA256:{pcb_hash}')
        lines.append('')

        lines.append(f'{name}_origin = [{minx:f}, {miny:f}];')
        lines.append(f'{name}_size = [{maxx - minx:f}, {maxy - miny:f}];')
        lines.append('')

        for ref, side, x, y, rot, val, package in objects:
            pkg, pargs = interpret_package(package)
            pargs = ', [' + ', '.join(str(v) for v in pargs) + ']' if pargs else ''
            #packages.add(pkgdata)
            lines.append(f'{name}_pos_{ref} = [{x:f}, {y:f}];')
            lines.append(f'{name}_rot_{ref} = {rot:f};')

            lines.append(f'{name}_data_{ref} = ["{ref}", {name}_pos_{ref}, {name}_rot_{ref}, "{side}", "{val}", "{pkg}"{pargs}];')
            lines.append('')

        lines.append(f'{name}_objects = [')
        for ref, side, x, y, rot, val, package in objects:
            lines.append(f'    {name}_data_{ref},')

        lines.append(f'];')
        lines.append('')

        all_hole_defs = []
        for ((hole_size, plated), oval), hole_list in holes.items():
            hole_size_vname = str(hole_size).replace('.', '_')
            varname = f'{name}_drill_{hole_size_vname}_' + ('pth' if plated else 'npth')
            if oval:
                varname += '_oval'

            all_hole_defs.append(f'    each [for(pt = {varname}) [pt, {hole_size}]],')
            lines.append(f'{varname} = [')
            if oval:
                for x1, y1, x2, y2 in hole_list:
                    lines.append(f'    [[{x1 - minx:f}, {y1 - miny:f}], [{x2 - minx:f}, {y2 - miny:f}]],')

            else:
                for x, y in hole_list:
                    lines.append(f'    [{x - minx:f}, {y - miny:f}],')
            lines.append(f'];')
            lines.append('')

        lines.append(f'{name}_drill = [')
        for line in all_hole_defs:
            lines.append(line)
        lines.append(f'];')

        lines.append(module_base % {'name': name, 'fname': fname})

        scad_text = '\n'.join(lines)
        (args.outdir / f'{fname}-pcb-model.scad').write_text(scad_text)

        if args.stl:
            for fn in ('consts.scad', 'kicad.scad', 'shapes.scad', 'funcs.scad'):
                shutil.copy2(Path(__file__).with_name(fn), tempd / fn)
            for fn in ('kicad-models.scad', 'kicad-models-wrl.scad'):
                (tempd / fn).write_text('')

            (tempd / 'gen.scad').write_text(f'use <kicad.scad>\n{scad_text}\n{name}_pcb_model_part(export_part, $fn=15);\n')


            procargs = []
            for part in ('board', 'trace-f', 'trace-b', 'mask-f', 'mask-b'):
                fn = f'{fname}-pcb-{part}.dxf'
                shutil.copy2(args.outdir / fn, tempd / fn)

                procargs.append([
                    'openscad', tempd / 'gen.scad',
                    f'-Dexport_part="{part}"', '-o',
                    args.outdir / f'{fname}-pcb-{part}.stl',
                    '--export-format=binstl',
                    '--hardwarnings'

                ])

            procs = [subprocess.Popen(scadargs) for scadargs in procargs]
            for proc in procs:
                proc.wait()


        for pkg in sorted(packages):
            print(pkg)

if __name__ == '__main__':
    main()
