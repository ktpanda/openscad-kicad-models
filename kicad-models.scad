include <../ktpanda/consts.scad>
use <../ktpanda/funcs.scad>
use <../ktpanda/shapes.scad>
use <wrl-models/kicad-models-wrl.scad>

// Hand-created models for components

module capacitor_base(d, h) {
    render(4)
    difference() {
        intersection() {
            //cylx(0, h, d=d);
            minkowski() {
                cylx(.5, h-.5, d=d - 1);
                sphere(d=1, $fn = 10);

            }
        }
        tz(1.3)
        rotate_extrude() tx(d/2 + 1) circle(1.4);
    }
}

module relay_songle_srd() {
    pins = [
        [-6, -2],
        [-6, -14.2],
        [0, 0],
        [6, -2],
        [6, -14.2],
    ];

    ww = 15.5;
    ll = 19.1;
    hh = 15.3;

    rz(180)
    ty(-1.7)
    difference() {
        color("#5555ff")
        render(5)
        intersection() {

            minkowski() {
                cubex(
                    xc=0, y1=.5, z1=-2,
                    xs=ww-1,
                    ys=ll-1,
                    z2=hh-1
                );
                sphere(d=1, $fn=8);
            }
            cubex(
                xc=0, y1=0, z1=0,
                xs=ww,
                ys=ll,
                zs=hh
            );

        }
        color("#222222")
        cubex(
            xc=0, y1=.4, z1=-1,
            xs=ww - .8,
            y2=ll-.4, z2=.4,
            r=.5
        );
    }
    color("#cccccc")
    for (pin = pins) translate(pin) {
        cylx(-2, 0, d=0.8);
    }
}

module electrolytic_capacitor(d=10, h=10, pin_spacing=3.5, pin_d=0.4) {
    bend_z = abs(sin($component_bend) * d/2);
    tz(bend_z)
    rx($component_bend)
    tz($component_bend != 0 ? $pin_length : 0) {
        tx(pin_spacing/2)
        difference() {
            color_part(["#222277", "#bbbbff"]) {
                capacitor_base(d, h);

                tx(d/2)
                difference() {
                    // stripe
                    cubex(
                        xc=0, xs=3,
                        yc=0, ys=d/3,
                        z1=.2, z2=h+1
                    );

                    // minus sign
                    tz(h/2)
                    yzx() linehull([[0, -1], [0, 1]]) cylx(-3, 3, d=0.8);

                    // arrows on stripe
                    tz([1.3, h-2]) {
                        for (r=[-45, 45]) rx(r)
                        cubex(
                            xc=0, xs=3,
                            yc=0, ys=.5,
                            z1=-.25, z2=10
                        );
                    }
                }
            }

            color("#cccccc") {
                cylx(h-.2, h+.2, d=d-2);
                for (r=[0, 90]) rz(r) {
                    linehull([[-d/2+1.4, 0], [d/2-1.4, 0]]) cylx(h-.4, h+.2, d=.5);
                }
            }
            color("#222222") {
                cylx(-1, 0.3, d=pin_spacing + pin_d/2 + .5);
            }
        }
        tx([0, pin_spacing]) {
            color("#cccccc")
            cylx(-$pin_length, 1, d=pin_d);
        }
    }
    if ($component_bend != 0) {
        tx([0, pin_spacing])
        tz(bend_z) {
            sphere(d=pin_d);
            cylx(-bend_z - 2, 0, d=pin_d);

        }

    }
}

module pushbutton() {
    color("#dddddd")
    cubex(
        xc=0, yc=0,
        xs=6, ys=6,
        z1=0, z2=3.5
    );
    color("#222222")
    cylx(1, 5, d=3);
}

module barrel_jack() {
    ww = 8.9;
    hh = 10.8;
    ll = 14.3;
    hh2 = 10.0;
    id  = 6.3;
    rz(180) {
        color("#333333")
        difference() {
            union() {
                cubex(
                    x1=ll-3.6, x2=ll,
                    yc=0, ys=ww,
                    z1=0, z2=hh
                );
                yzx()
                cubex(
                    z1=0, z2=ll-3.6,
                    xc=0, xs=ww,
                    y1=0, y2=hh2,
                    r3=ww/2, r4=ww/2

                );
            }
            tz(3 + id / 2) yzx() cylx(4, ll+eps, d=id);
        }

        color("#cccccc")
        tz(3 + id / 2) yzx() {
            cylx(4, 4.5, d=4);
            cylx(4, ll-2, d=2);

            tz(ll-2)
            sphere(d=2);

            cylx(-0.2, 0, d=3);
            linehull([[0, 0], [0, -9]]) cylx(-0.2, 0, d=1);

            tz(3) tx(ww/2) ry(-90)
            linehull([[0, -4], [0, -9]]) cylx(-0.2, 0, d=1);

            tz(6.2)
            linehull([[0, -4], [0, -9]]) cylx(-0.2, 0, d=1);

        }
    }
}

module power_mosfet() {
    // main body
    pt = 0.6;
    rx($mosfet_bend)
    translate([2.54, 2.52, 3]) {
        difference() {
            color("#222222")
            cubex(
                xc=0, y1=0, z1=0,
                xs=10,
                ys=-4.5,
                zs=9
            );
            color("#cccccc") {
                cubex(
                    xc=0, yc=0, z1=2.54,
                    xs=8,
                    ys=0.01,
                    z2=8
                );

                cubex(
                    xc=0, yc=0, z1=2.54,
                    xs=6,
                    ys=0.01,
                    z2=10
                );
            }
        }

        // heat sink
        tz(9)
        color("#cccccc")
        difference() {
            cubex(
                xc=0, y1=0, z1=0,
                xs=10,
                ys=-1.2,
                zs=6.2
            );
            rfx() tx(-5) {
                cubex(
                    x1=-eps, x2=0.6,
                    y1=-2, y2=2,
                    z1=3.4,
                    z2=6.2 - 1.35
                );
            }

            tz(3.45)
            xzy() cylx(-2, 2, d=3.6);

        }
    }

    //pins
    color("#cccccc")
    for (pn = [0 : 2]) tx(pn * 2.54) {
        rx($mosfet_bend)
        xzy() linehull([[0, 0.5], [0, 3]]) cylinder(h=pt, center=true, d=1.3);
        xzy() linehull([[0, 0.5], [0, -3]]) cylinder(h=pt, center=true, d=0.8);
    }
}

module pin_xsect(pd) {
    intersection() {
        square(pd, center=true);
        rz(45) square(sqrt(2) * pd * .85, center=true);

    }
}

module pinheader_horiz(n=4, r=1, pd=0.63, pl=10) {
    bend_rad = pd*.75+1;
    h1 = 1.27;

    color("gold")
    for (xx = [0 : r-1], yy=[0 : n-1]) {
        tx(xx * 2.54) ty(-yy*2.54)  {
            h = (r - xx - 1) * 2.54;
            tz(-2) linear_extrude(h1 - bend_rad + h + 2) pin_xsect(pd);
            //cylx(-2, , d=pd);

            tz(h1 + h - bend_rad) tx(bend_rad)
            xzy() rz(90) rotate_extrude(angle=90) tx(bend_rad) pin_xsect(pd);

            lpl = -h - pl + bend_rad;
            tz(h1 + h - bend_rad) tx(bend_rad) tz(bend_rad) yzx()
            linear_extrude(-lpl) pin_xsect(pd);

        }
    }
    color("#222222")
    cubex(
        xc=3 + (r-1) * 2.54, xs=2.5,
        y1=-h1 - (n-1) * 2.54, y2=h1,
        z1=0, z2=h1 + (r-1) * 2.54 + h1
    );
    //echo(h1 + (r-1) * 2.54 + h1);
}

module pinheader_vert(n=4, r=1, pd=0.63, pl=8.3) {
    h1 = 1.27;

    color("gold")
    for (xx = [0 : r-1], yy=[0 : n-1]) {
        tx(xx * 2.54) ty(-yy*2.54)  {
            tz(-2) linear_extrude(pl + 2) pin_xsect(pd);
        }
    }
    color("#222222")
    cubex(
        z1=0, zs=2.5,
        y1=-h1 - (n-1) * 2.54, y2=h1,
        x1=-h1, x2=h1 + (r-1) * 2.54
    );
}

module screw_terminal(n=2) {
    l = 5.08 * n;
    tx(-2.54)
    color("#4444ff")
    difference() {
        yzx() lxpoly(l, points=[
            [4.3, 0],
            [-3.5, 0],
            [-3.5, 6.25],
            [-2.5, 10],
            [-2.5+5.2, 10],
            [4.3, 6.25],
        ]);
        tx(2.54)
        for (i = [0 : n-1]) tx(5.08 * i) {
            cylx(8, 10+eps,d=3.4);
            cubex(
                xc=0, xs=4.3,
                y1=-3.6, y2=4.2,
                z1=1, zs=5
            );
        }
    }
    for (i = [0 : n-1]) tx(5.08 * i) {
        difference() {
            color("#e0e0e0")
            cylx(8, 9, d=3.2);
            color("#c0c0c0")
            for (ang = [-45, 45]) rz(ang) {
                linehull([[-1, 0], [1, 0]]) cylx(8.8, 10, d=0.3);
            }

        }
        color("#e0e0e0")
        yzx() cubex(xc=0, xs=1, zc=0, zs=0.64, r=.5, y1=-3, y2=0);

        color("#e0e0e0")
        difference() {
            xzy()
            cubex(
                xc=0, xs=4,
                z1=-2.5, z2=4.1,
                y1=1, ys=5,
                r=1
            );
            xzy()
            cubex(
                xc=0, xs=3,
                z1=-2.6, z2=4,
                y1=2, ys=3,
                r=1
            );
        }
    }
}

module pinsocket_vert(n=4, r=1, pd=0.63, pl=8.3, pins=true) {
    h1 = 1.27;

    tx(-2.54*(r-1)) {
        if (pins)
        color("gold")
        for (xx = [0 : r-1], yy=[0 : n-1]) {
            tx(xx * 2.54) ty(-yy*2.54) {
                tz(-2) linear_extrude(4) pin_xsect(pd);
            }
        }
        color("#222222")
        difference() {
            cubex(
                z1=0, zs=pl,
                y1=-h1 - (n-1) * 2.54, y2=h1,
                x1=-h1, x2=h1 + (r-1) * 2.54
            );
            for (xx = [0 : r-1], yy=[0 : n-1]) {
                tx(xx * 2.54) ty(-yy*2.54) {
                    tz(1) linear_extrude(pl - 1 + eps) pin_xsect(pd+.4);
                    tz(pl - .6) linear_extrude(.6+eps, scale=1.6) pin_xsect(pd+.4);
                }
            }
        }
    }
}

module spade_terminal(size=6.35, r=1, n=1, spc=5.08) {
    for (yy = [0 : n-1]) ty(-spc*yy) {
        color("#e0e0e0")
        intersection() {
            xzy() linear_extrude(0.8, center=true, convexity=10) difference() {
                rfx(2.54)
                polygon([
                    [-1.42, 0],
                    [-.46, 0],
                    [-.46, -3.5],
                    [.46, -3.5],
                    [.46, 1],
                    [2.54, 1],
                    [2.54, 9.5],

                    [-.635+1.64, 9.5],
                    [-.635, 9.5-1.64],

                    [-.635, 2.0],
                    [-1.42, 2.0],
                ]);
                tx(2.54)
                ty(5.2)
                circle(d=1.8);
            }
            tx(-1.5)
            yzx() linear_extrude(8) polygon([
                [-.375, -3.5],
                [.375, -3.5],
                [.375, 9],
                [.2, 9.5],
                [-.2, 9.5],
                [-.375, 9],
            ]);
        }
    }
}

module smd_diode(size) {
    if (size == "0603") kicad_d_0603_1608metric();
    if (size == "0805") kicad_d_0805_2012metric();
    if (size == "1206") kicad_d_1206_3216metric();
    if (size == "2010") kicad_d_2010_5025metric();
    if (size == "2114") kicad_d_2114_3652metric();
    if (size == "3220") kicad_d_3220_8050metric();
}

module smd_resistor(size) {
    if (size == "0201") kicad_r_0201_0603metric();
    if (size == "0402") kicad_r_0402_1005metric();
    if (size == "0603") kicad_r_0603_1608metric();
    if (size == "0612") kicad_r_0612_1632metric();
    if (size == "0805") kicad_r_0805_2012metric();
    if (size == "0815") kicad_r_0815_2038metric();
    if (size == "1020") kicad_r_1020_2550metric();
    if (size == "1206") kicad_r_1206_3216metric();
    if (size == "1210") kicad_r_1210_3225metric();
    if (size == "1218") kicad_r_1218_3246metric();
    if (size == "1806") kicad_r_1806_4516metric();
    if (size == "1812") kicad_r_1812_4532metric();
    if (size == "2010") kicad_r_2010_5025metric();
    if (size == "2512") kicad_r_2512_6332metric();
    if (size == "2816") kicad_r_2816_7142metric();
    if (size == "4020") kicad_r_4020_10251metric();
}

module smd_capacitor(size) {
    if (size == "0201") kicad_c_0201_0603metric();
    if (size == "0402") kicad_c_0402_1005metric();
    if (size == "0603") kicad_c_0603_1608metric();
    if (size == "0805") kicad_c_0805_2012metric();
    if (size == "1206") kicad_c_1206_3216metric();
    if (size == "1210") kicad_c_1210_3225metric();
    if (size == "1806") kicad_c_1806_4516metric();
    if (size == "1812") kicad_c_1812_4532metric();
    if (size == "1825") kicad_c_1825_4564metric();
    if (size == "2010") kicad_c_2010_5025metric();
    if (size == "2220") kicad_c_2220_5650metric();
    if (size == "2225") kicad_c_2225_5664metric();
    if (size == "2512") kicad_c_2512_6332metric();
    if (size == "2816") kicad_c_2816_7142metric();
    if (size == "3640") kicad_c_3640_9110metric();
}

module smd_led(size) {
    if (size == "0201") kicad_led_0201_0603metric();
    if (size == "0402") kicad_led_0402_1005metric();
    if (size == "0603") kicad_led_0603_1608metric();
    if (size == "0805") kicad_led_0805_2012metric();
    if (size == "1206") kicad_led_1206_3216metric();
    if (size == "1210") kicad_led_1210_3225metric();
    if (size == "1806") kicad_led_1806_4516metric();
    if (size == "1812") kicad_led_1812_4532metric();
    if (size == "2010") kicad_led_2010_5025metric();
    if (size == "2512") kicad_led_2512_6332metric();
    if (size == "2816") kicad_led_2816_7142metric();
}

module smd_qfn(pins, w, h) {
    if (pins == 12 && w == 3 && h == 3) kicad_qfn_12_1ep_3x3mm_p0_5mm_ep1_65x1_65mm();
    if (pins == 16 && w == 3 && h == 3) kicad_qfn_16_1ep_3x3mm_p0_5mm_ep1_8x1_8mm();
    if (pins == 16 && w == 4 && h == 4) kicad_qfn_16_1ep_4x4mm_p0_65mm_ep2_1x2_1mm();
    if (pins == 16 && w == 5 && h == 5) kicad_qfn_16_1ep_5x5mm_p0_8mm_ep2_7x2_7mm();
    if (pins == 20 && w == 3 && h == 3) kicad_qfn_20_1ep_3x3mm_p0_45mm_ep1_6x1_6mm();
    if (pins == 20 && w == 3 && h == 4) kicad_qfn_20_1ep_3x4mm_p0_5mm_ep1_65x2_65mm();
    if (pins == 20 && w == 4 && h == 4) kicad_qfn_20_1ep_4x4mm_p0_5mm_ep2_25x2_25mm();
    if (pins == 20 && w == 4 && h == 5) kicad_qfn_20_1ep_4x5mm_p0_5mm_ep2_65x3_65mm();
    if (pins == 20 && w == 5 && h == 5) kicad_qfn_20_1ep_5x5mm_p0_65mm_ep3_35x3_35mm();
    if (pins == 24 && w == 3 && h == 3) kicad_qfn_24_1ep_3x3mm_p0_4mm_ep1_75x1_6mm();
    if (pins == 24 && w == 3 && h == 4) kicad_qfn_24_1ep_3x4mm_p0_4mm_ep1_65x2_65mm();
    if (pins == 24 && w == 4 && h == 4) kicad_qfn_24_1ep_4x4mm_p0_5mm_ep2_6x2_6mm();
    if (pins == 24 && w == 4 && h == 5) kicad_qfn_24_1ep_4x5mm_p0_5mm_ep2_65x3_65mm();
    if (pins == 24 && w == 5 && h == 5) kicad_qfn_24_1ep_5x5mm_p0_65mm_ep3_2x3_2mm();
    if (pins == 28 && w == 3 && h == 6) kicad_qfn_28_1ep_3x6mm_p0_5mm_ep1_7x4_75mm();
    if (pins == 28 && w == 4 && h == 4) kicad_qfn_28_1ep_4x4mm_p0_45mm_ep2_4x2_4mm();
    if (pins == 28 && w == 4 && h == 5) kicad_qfn_28_1ep_4x5mm_p0_5mm_ep2_65x3_25mm();
    if (pins == 28 && w == 5 && h == 5) kicad_qfn_28_1ep_5x5mm_p0_5mm_ep3_35x3_35mm();
    if (pins == 28 && w == 5 && h == 6) kicad_qfn_28_1ep_5x6mm_p0_5mm_ep3_65x4_65mm();
    if (pins == 28 && w == 6 && h == 6) kicad_qfn_28_1ep_6x6mm_p0_65mm_ep4_25x4_25mm();
    if (pins == 32 && w == 4 && h == 4) kicad_qfn_32_1ep_4x4mm_p0_4mm_ep2_65x2_65mm();
    if (pins == 32 && w == 5 && h == 5) kicad_qfn_32_1ep_5x5mm_p0_5mm_ep3_1x3_1mm();
    if (pins == 32 && w == 7 && h == 7) kicad_qfn_32_1ep_7x7mm_p0_65mm_ep4_65x4_65mm();
    if (pins == 36 && w == 5 && h == 6) kicad_qfn_36_1ep_5x6mm_p0_5mm_ep3_6x4_1mm();
    if (pins == 36 && w == 6 && h == 6) kicad_qfn_36_1ep_6x6mm_p0_5mm_ep3_7x3_7mm();
    if (pins == 38 && w == 4 && h == 6) kicad_qfn_38_1ep_4x6mm_p0_4mm_ep2_65x4_65mm();
    if (pins == 38 && w == 5 && h == 7) kicad_qfn_38_1ep_5x7mm_p0_5mm_ep3_15x5_15mm();
    if (pins == 40 && w == 5 && h == 5) kicad_qfn_40_1ep_5x5mm_p0_4mm_ep3_6x3_6mm();
    if (pins == 40 && w == 6 && h == 6) kicad_qfn_40_1ep_6x6mm_p0_5mm_ep4_6x4_6mm();
    if (pins == 42 && w == 5 && h == 6) kicad_qfn_42_1ep_5x6mm_p0_4mm_ep3_7x4_7mm();
    if (pins == 44 && w == 7 && h == 7) kicad_qfn_44_1ep_7x7mm_p0_5mm_ep5_15x5_15mm();
    if (pins == 44 && w == 8 && h == 8) kicad_qfn_44_1ep_8x8mm_p0_65mm_ep6_45x6_45mm();
    if (pins == 44 && w == 9 && h == 9) kicad_qfn_44_1ep_9x9mm_p0_65mm_ep7_5x7_5mm();
    if (pins == 48 && w == 5 && h == 5) kicad_qfn_48_1ep_5x5mm_p0_35mm_ep3_7x3_7mm();
    if (pins == 48 && w == 6 && h == 6) kicad_qfn_48_1ep_6x6mm_p0_4mm_ep4_3x4_3mm();
    if (pins == 48 && w == 7 && h == 7) kicad_qfn_48_1ep_7x7mm_p0_5mm_ep5_15x5_15mm();
    if (pins == 52 && w == 7 && h == 8) kicad_qfn_52_1ep_7x8mm_p0_5mm_ep5_41x6_45mm();
    if (pins == 56 && w == 7 && h == 7) kicad_qfn_56_1ep_7x7mm_p0_4mm_ep3_2x3_2mm();
    if (pins == 56 && w == 8 && h == 8) kicad_qfn_56_1ep_8x8mm_p0_5mm_ep4_5x5_2mm();
    if (pins == 64 && w == 8 && h == 8) kicad_qfn_64_1ep_8x8mm_p0_4mm_ep6_5x6_5mm();
    if (pins == 64 && w == 9 && h == 9) kicad_qfn_64_1ep_9x9mm_p0_5mm_ep4_7x4_7mm();
    if (pins == 68 && w == 8 && h == 8) kicad_qfn_68_1ep_8x8mm_p0_4mm_ep5_2x5_2mm();
    if (pins == 76 && w == 9 && h == 9) kicad_qfn_76_1ep_9x9mm_p0_4mm_ep3_8x3_8mm();
}

module smd_soic(pins, wide, w, h, spc) {
    if(pins == 14 && wide == 1 && w == 7.5 && h == 9.0 && spc == 1.27) { kicad_soic_14w_7_5x9_0mm_p1_27mm(); }
    else if(pins == 14 && wide == 0 && w == 3.9 && h == 8.7 && spc == 1.27) { kicad_soic_14_3_9x8_7mm_p1_27mm(); }
    else if(pins == 16 && wide == 1 && w == 5.3 && h == 10.2 && spc == 1.27) { kicad_soic_16w_5_3x10_2mm_p1_27mm(); }
    else if(pins == 16 && wide == 1 && w == 7.5 && h == 10.3 && spc == 1.27) { kicad_soic_16w_7_5x10_3mm_p1_27mm(); }
    else if(pins == 16 && wide == 1 && w == 7.5 && h == 12.8 && spc == 1.27) { kicad_soic_16w_7_5x12_8mm_p1_27mm(); }
    else if(pins == 16 && wide == 0 && w == 3.9 && h == 9.9 && spc == 1.27) { kicad_soic_16_3_9x9_9mm_p1_27mm(); }
    else if(pins == 16 && wide == 0 && w == 4.55 && h == 10.3 && spc == 1.27) { kicad_soic_16_4_55x10_3mm_p1_27mm(); }
    else if(pins == 18 && wide == 1 && w == 7.5 && h == 11.6 && spc == 1.27) { kicad_soic_18w_7_5x11_6mm_p1_27mm(); }
    else if(pins == 20 && wide == 1 && w == 7.5 && h == 12.8 && spc == 1.27) { kicad_soic_20w_7_5x12_8mm_p1_27mm(); }
    else if(pins == 24 && wide == 1 && w == 7.5 && h == 15.4 && spc == 1.27) { kicad_soic_24w_7_5x15_4mm_p1_27mm(); }
    else if(pins == 28 && wide == 1 && w == 7.5 && h == 17.9 && spc == 1.27) { kicad_soic_28w_7_5x17_9mm_p1_27mm(); }
    else if(pins == 28 && wide == 1 && w == 7.5 && h == 18.7 && spc == 1.27) { kicad_soic_28w_7_5x18_7mm_p1_27mm(); }
    else if(pins == 8 && wide == 0 && w == 3.9 && h == 4.9 && spc == 1.27) { kicad_soic_8_3_9x4_9mm_p1_27mm(); }
    else if(pins == 8 && wide == 0 && w == 5.275 && h == 5.275 && spc == 1.27) { kicad_soic_8_5_275x5_275mm_p1_27mm(); }
}

module smd_ssop(pins, w, h, spc) {
    if(pins == 10 && w == 3.9 && h == 4.9 && spc == 1.00) { kicad_ssop_10_3_9x4_9mm_p1_00mm(); }
    else if(pins == 14 && w == 5.3 && h == 6.2 && spc == 0.65) { kicad_ssop_14_5_3x6_2mm_p0_65mm(); }
    else if(pins == 16 && w == 3.9 && h == 4.9 && spc == 0.635) { kicad_ssop_16_3_9x4_9mm_p0_635mm(); }
    else if(pins == 16 && w == 4.4 && h == 5.2 && spc == 0.65) { kicad_ssop_16_4_4x5_2mm_p0_65mm(); }
    else if(pins == 16 && w == 5.3 && h == 6.2 && spc == 0.65) { kicad_ssop_16_5_3x6_2mm_p0_65mm(); }
    else if(pins == 18 && w == 4.4 && h == 6.5 && spc == 0.65) { kicad_ssop_18_4_4x6_5mm_p0_65mm(); }
    else if(pins == 20 && w == 3.9 && h == 8.7 && spc == 0.635) { kicad_ssop_20_3_9x8_7mm_p0_635mm(); }
    else if(pins == 20 && w == 4.4 && h == 6.5 && spc == 0.65) { kicad_ssop_20_4_4x6_5mm_p0_65mm(); }
    else if(pins == 20 && w == 5.3 && h == 7.2 && spc == 0.65) { kicad_ssop_20_5_3x7_2mm_p0_65mm(); }
    else if(pins == 24 && w == 3.9 && h == 8.7 && spc == 0.635) { kicad_ssop_24_3_9x8_7mm_p0_635mm(); }
    else if(pins == 24 && w == 5.3 && h == 8.2 && spc == 0.65) { kicad_ssop_24_5_3x8_2mm_p0_65mm(); }
    else if(pins == 28 && w == 3.9 && h == 9.9 && spc == 0.635) { kicad_ssop_28_3_9x9_9mm_p0_635mm(); }
    else if(pins == 28 && w == 5.3 && h == 10.2 && spc == 0.65) { kicad_ssop_28_5_3x10_2mm_p0_65mm(); }
    else if(pins == 32 && w == 11.305 && h == 20.495 && spc == 1.27) { kicad_ssop_32_11_305x20_495mm_p1_27mm(); }
    else if(pins == 44 && w == 5.3 && h == 12.8 && spc == 0.5) { kicad_ssop_44_5_3x12_8mm_p0_5mm(); }
    else if(pins == 48 && w == 7.5 && h == 15.9 && spc == 0.635) { kicad_ssop_48_7_5x15_9mm_p0_635mm(); }
    else if(pins == 56 && w == 7.5 && h == 18.5 && spc == 0.635) { kicad_ssop_56_7_5x18_5mm_p0_635mm(); }
    else if(pins == 8 && w == 2.95 && h == 2.8 && spc == 0.65) { kicad_ssop_8_2_95x2_8mm_p0_65mm(); }
    else if(pins == 8 && w == 3.9 && h == 5.05 && spc == 1.27) { kicad_ssop_8_3_9x5_05mm_p1_27mm(); }
    else if(pins == 8 && w == 5.25 && h == 5.24 && spc == 1.27) { kicad_ssop_8_5_25x5_24mm_p1_27mm(); }
}

module smd_tssop(pins, w, h, spc) {
    if(pins == 10 && w == 3 && h == 3 && spc == 0.5) { kicad_tssop_10_3x3mm_p0_5mm(); }
    else if(pins == 14 && w == 4.4 && h == 5 && spc == 0.65) { kicad_tssop_14_4_4x5mm_p0_65mm(); }
    else if(pins == 16 && w == 4.4 && h == 5 && spc == 0.65) { kicad_tssop_16_4_4x5mm_p0_65mm(); }
    else if(pins == 20 && w == 4.4 && h == 5 && spc == 0.5) { kicad_tssop_20_4_4x5mm_p0_5mm(); }
    else if(pins == 20 && w == 4.4 && h == 6.5 && spc == 0.65) { kicad_tssop_20_4_4x6_5mm_p0_65mm(); }
    else if(pins == 24 && w == 4.4 && h == 5 && spc == 0.4) { kicad_tssop_24_4_4x5mm_p0_4mm(); }
    else if(pins == 24 && w == 4.4 && h == 6.5 && spc == 0.5) { kicad_tssop_24_4_4x6_5mm_p0_5mm(); }
    else if(pins == 24 && w == 4.4 && h == 7.8 && spc == 0.65) { kicad_tssop_24_4_4x7_8mm_p0_65mm(); }
    else if(pins == 24 && w == 6.1 && h == 7.8 && spc == 0.65) { kicad_tssop_24_6_1x7_8mm_p0_65mm(); }
    else if(pins == 28 && w == 4.4 && h == 7.8 && spc == 0.5) { kicad_tssop_28_4_4x7_8mm_p0_5mm(); }
    else if(pins == 28 && w == 4.4 && h == 9.7 && spc == 0.65) { kicad_tssop_28_4_4x9_7mm_p0_65mm(); }
    else if(pins == 28 && w == 6.1 && h == 7.8 && spc == 0.5) { kicad_tssop_28_6_1x7_8mm_p0_5mm(); }
    else if(pins == 28 && w == 6.1 && h == 9.7 && spc == 0.65) { kicad_tssop_28_6_1x9_7mm_p0_65mm(); }
    else if(pins == 28 && w == 8 && h == 9.7 && spc == 0.65) { kicad_tssop_28_8x9_7mm_p0_65mm(); }
    else if(pins == 30 && w == 4.4 && h == 7.8 && spc == 0.5) { kicad_tssop_30_4_4x7_8mm_p0_5mm(); }
    else if(pins == 30 && w == 6.1 && h == 9.7 && spc == 0.65) { kicad_tssop_30_6_1x9_7mm_p0_65mm(); }
    else if(pins == 32 && w == 4.4 && h == 6.5 && spc == 0.4) { kicad_tssop_32_4_4x6_5mm_p0_4mm(); }
    else if(pins == 32 && w == 6.1 && h == 11 && spc == 0.65) { kicad_tssop_32_6_1x11mm_p0_65mm(); }
    else if(pins == 32 && w == 8 && h == 11 && spc == 0.65) { kicad_tssop_32_8x11mm_p0_65mm(); }
    else if(pins == 36 && w == 4.4 && h == 7.8 && spc == 0.4) { kicad_tssop_36_4_4x7_8mm_p0_4mm(); }
    else if(pins == 36 && w == 4.4 && h == 9.7 && spc == 0.5) { kicad_tssop_36_4_4x9_7mm_p0_5mm(); }
    else if(pins == 36 && w == 6.1 && h == 12.5 && spc == 0.65) { kicad_tssop_36_6_1x12_5mm_p0_65mm(); }
    else if(pins == 36 && w == 6.1 && h == 7.8 && spc == 0.4) { kicad_tssop_36_6_1x7_8mm_p0_4mm(); }
    else if(pins == 36 && w == 6.1 && h == 9.7 && spc == 0.5) { kicad_tssop_36_6_1x9_7mm_p0_5mm(); }
    else if(pins == 36 && w == 8 && h == 12.5 && spc == 0.65) { kicad_tssop_36_8x12_5mm_p0_65mm(); }
    else if(pins == 36 && w == 8 && h == 9.7 && spc == 0.5) { kicad_tssop_36_8x9_7mm_p0_5mm(); }
    else if(pins == 38 && w == 4.4 && h == 9.7 && spc == 0.5) { kicad_tssop_38_4_4x9_7mm_p0_5mm(); }
    else if(pins == 38 && w == 6.1 && h == 12.5 && spc == 0.65) { kicad_tssop_38_6_1x12_5mm_p0_65mm(); }
    else if(pins == 40 && w == 6.1 && h == 11 && spc == 0.5) { kicad_tssop_40_6_1x11mm_p0_5mm(); }
    else if(pins == 40 && w == 6.1 && h == 14 && spc == 0.65) { kicad_tssop_40_6_1x14mm_p0_65mm(); }
    else if(pins == 40 && w == 8 && h == 11 && spc == 0.5) { kicad_tssop_40_8x11mm_p0_5mm(); }
    else if(pins == 40 && w == 8 && h == 14 && spc == 0.65) { kicad_tssop_40_8x14mm_p0_65mm(); }
    else if(pins == 44 && w == 4.4 && h == 11.2 && spc == 0.5) { kicad_tssop_44_4_4x11_2mm_p0_5mm(); }
    else if(pins == 44 && w == 4.4 && h == 11 && spc == 0.5) { kicad_tssop_44_4_4x11mm_p0_5mm(); }
    else if(pins == 44 && w == 6.1 && h == 11 && spc == 0.5) { kicad_tssop_44_6_1x11mm_p0_5mm(); }
    else if(pins == 48 && w == 4.4 && h == 9.7 && spc == 0.4) { kicad_tssop_48_4_4x9_7mm_p0_4mm(); }
    else if(pins == 48 && w == 6.1 && h == 12.5 && spc == 0.5) { kicad_tssop_48_6_1x12_5mm_p0_5mm(); }
    else if(pins == 48 && w == 6.1 && h == 9.7 && spc == 0.4) { kicad_tssop_48_6_1x9_7mm_p0_4mm(); }
    else if(pins == 48 && w == 8 && h == 12.5 && spc == 0.5) { kicad_tssop_48_8x12_5mm_p0_5mm(); }
    else if(pins == 48 && w == 8 && h == 9.7 && spc == 0.4) { kicad_tssop_48_8x9_7mm_p0_4mm(); }
    else if(pins == 50 && w == 4.4 && h == 12.5 && spc == 0.5) { kicad_tssop_50_4_4x12_5mm_p0_5mm(); }
    else if(pins == 52 && w == 6.1 && h == 11 && spc == 0.4) { kicad_tssop_52_6_1x11mm_p0_4mm(); }
    else if(pins == 52 && w == 8 && h == 11 && spc == 0.4) { kicad_tssop_52_8x11mm_p0_4mm(); }
    else if(pins == 56 && w == 6.1 && h == 12.5 && spc == 0.4) { kicad_tssop_56_6_1x12_5mm_p0_4mm(); }
    else if(pins == 56 && w == 6.1 && h == 14 && spc == 0.5) { kicad_tssop_56_6_1x14mm_p0_5mm(); }
    else if(pins == 56 && w == 8 && h == 12.5 && spc == 0.4) { kicad_tssop_56_8x12_5mm_p0_4mm(); }
    else if(pins == 56 && w == 8 && h == 14 && spc == 0.5) { kicad_tssop_56_8x14mm_p0_5mm(); }
    else if(pins == 60 && w == 8 && h == 12.5 && spc == 0.4) { kicad_tssop_60_8x12_5mm_p0_4mm(); }
    else if(pins == 64 && w == 6.1 && h == 14 && spc == 0.4) { kicad_tssop_64_6_1x14mm_p0_4mm(); }
    else if(pins == 64 && w == 6.1 && h == 17 && spc == 0.5) { kicad_tssop_64_6_1x17mm_p0_5mm(); }
    else if(pins == 64 && w == 8 && h == 14 && spc == 0.4) { kicad_tssop_64_8x14mm_p0_4mm(); }
    else if(pins == 68 && w == 8 && h == 14 && spc == 0.4) { kicad_tssop_68_8x14mm_p0_4mm(); }
    else if(pins == 80 && w == 6.1 && h == 17 && spc == 0.4) { kicad_tssop_80_6_1x17mm_p0_4mm(); }
    else if(pins == 8 && w == 3 && h == 3 && spc == 0.65) { kicad_tssop_8_3x3mm_p0_65mm(); }
    else if(pins == 8 && w == 4.4 && h == 3 && spc == 0.65) { kicad_tssop_8_4_4x3mm_p0_65mm(); }
}

module tht_dip(pins, w) {
    if(pins == 10 && w == 10.16) { kicad_dip_10_w10_16mm(); }
    else if(pins == 10 && w == 7.62) { kicad_dip_10_w7_62mm(); }
    else if(pins == 12 && w == 10.16) { kicad_dip_12_w10_16mm(); }
    else if(pins == 12 && w == 7.62) { kicad_dip_12_w7_62mm(); }
    else if(pins == 14 && w == 10.16) { kicad_dip_14_w10_16mm(); }
    else if(pins == 14 && w == 7.62) { kicad_dip_14_w7_62mm(); }
    else if(pins == 16 && w == 10.16) { kicad_dip_16_w10_16mm(); }
    else if(pins == 16 && w == 7.62) { kicad_dip_16_w7_62mm(); }
    else if(pins == 18 && w == 7.62) { kicad_dip_18_w7_62mm(); }
    else if(pins == 20 && w == 7.62) { kicad_dip_20_w7_62mm(); }
    else if(pins == 22 && w == 10.16) { kicad_dip_22_w10_16mm(); }
    else if(pins == 22 && w == 7.62) { kicad_dip_22_w7_62mm(); }
    else if(pins == 24 && w == 10.16) { kicad_dip_24_w10_16mm(); }
    else if(pins == 24 && w == 15.24) { kicad_dip_24_w15_24mm(); }
    else if(pins == 24 && w == 7.62) { kicad_dip_24_w7_62mm(); }
    else if(pins == 28 && w == 15.24) { kicad_dip_28_w15_24mm(); }
    else if(pins == 28 && w == 7.62) { kicad_dip_28_w7_62mm(); }
    else if(pins == 32 && w == 15.24) { kicad_dip_32_w15_24mm(); }
    else if(pins == 32 && w == 7.62) { kicad_dip_32_w7_62mm(); }
    else if(pins == 40 && w == 15.24) { kicad_dip_40_w15_24mm(); }
    else if(pins == 40 && w == 25.4) { kicad_dip_40_w25_4mm(); }
    else if(pins == 42 && w == 15.24) { kicad_dip_42_w15_24mm(); }
    else if(pins == 48 && w == 15.24) { kicad_dip_48_w15_24mm(); }
    else if(pins == 4 && w == 10.16) { kicad_dip_4_w10_16mm(); }
    else if(pins == 4 && w == 7.62) { kicad_dip_4_w7_62mm(); }
    else if(pins == 64 && w == 15.24) { kicad_dip_64_w15_24mm(); }
    else if(pins == 64 && w == 22.86) { kicad_dip_64_w22_86mm(); }
    else if(pins == 64 && w == 25.4) { kicad_dip_64_w25_4mm(); }
    else if(pins == 6 && w == 10.16) { kicad_dip_6_w10_16mm(); }
    else if(pins == 6 && w == 7.62) { kicad_dip_6_w7_62mm(); }
    else if(pins == 8 && w == 10.16) { kicad_dip_8_w10_16mm(); }
    else if(pins == 8 && w == 7.62) { kicad_dip_8_w7_62mm(); }
}

// TODO: this is ugly
module wuerth_dual_usb_a_connector() {
    translate([3.5, 1.01]) rz(-90) {
        difference() {
            cubex(
                x1=-.4, x2=17,
                yc=0, ys=12.8,
                z1=0, z2=15.34
            );
            tz([0, 6.82]) {
                cubex(
                    x1=5, x2=17+eps,
                    yc=0, ys=11.5,
                    z1=2, zs=5.12
                );
            }
        }
        color("gold")
        tx([1.01, 1.01+2.62]) rfy() {
            ty(1) cylx(-2.84, 0, d=.9);
            ty(3.5) cylx(-2.84, 0, d=.9);

        }
    }
}
